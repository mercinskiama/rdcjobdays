<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Rdcjobdays | News</title>
                <!--==========================
                Links
                ============================-->
            @include('structure/linkUp')
    </head>

<body class="bg-light style-fashion">

    <!-- Preloader -->
    @include('structure/Preloader')

    <!-- Bg Overlay -->
    <div class="content-overlay"></div>

    <!-- Sidenav -->
    @include('structure/header')
    <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
        <div class="top-bar d-none d-lg-block">
            <div class="container">
                <div class="row">

                <!-- Top menu -->
                <div class="col-lg-6">
                    <ul class="top-menu">
                    <li><a href="#">About</a></li>
                    <li><a href="#">Advertise</a></li>
                    <li><a href="#">Contact</a></li>
                    </ul>
                </div>

                <!-- Socials -->
                <div class="col-lg-6">
                    <div class="socials nav__socials socials--nobase socials--white justify-content-end">
                    <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                        <i class="ui-facebook"></i>
                    </a>
                    <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                        <i class="ui-twitter"></i>
                    </a>
                    <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                        <i class="ui-google"></i>
                    </a>
                    <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                        <i class="ui-youtube"></i>
                    </a>
                    <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                        <i class="ui-instagram"></i>
                    </a>
                    </div>
                </div>

                </div>
            </div>
        </div>
    <!-- end top bar -->

    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="/" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Actualité
        </li>
      </ul>
    </div>


    <div class="main-container container" id="main-container">

      <!-- Entry Image -->
      <div class="entry__img-holder mb-40">
        <img src="{{asset('media/img/content/hero/'.$information->image1)}}" alt="" class="entry__img">
      </div>

      <!-- Content -->
      <div class="row">

        <!-- Post Content -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box content-box--top-offset">

            <!-- standard post -->
            <article class="entry mb-0">

              <div class="single-post__entry-header entry__header">
                <ul class="entry__meta">
                  <li class="entry__meta-category">
                    <a href="#">Publié</a>
                  </li>
                  <li class="entry__meta-date">
                  {{\Carbon\Carbon::parse($information->date_actu)->format('d M Y')}}
                  </li>
                </ul>
                <h1 class="single-post__entry-title">
                {{$information->titre_actu}}
                </h1>
                <div class="entry__meta-holder">
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>Par</span>
                      <a href="#">{{$information->auteur}}</a>
                    </li>
                  </ul>

                  <ul class="entry__meta">
                    <li class="entry__meta-views">
                      <i class="ui-eye"></i>
                      <span>1356</span>
                    </li>
                    <li class="entry__meta-comments">
                      <a href="#">
                        <i class="ui-chat-empty"></i>13
                      </a>
                    </li>
                  </ul>
                </div>
              </div> <!-- end entry header -->

              <div class="entry__article-wrap">

                <!-- Share -->
                <div class="entry__share">
                  <div class="sticky-col">
                    <div class="socials socials--rounded socials--large">
                      <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                        <i class="ui-facebook"></i>
                      </a>
                      <a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                        <i class="ui-twitter"></i>
                      </a>
                      <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                        <i class="ui-google"></i>
                      </a>
                      <a class="social social-pinterest" href="#" title="pinterest" target="_blank" aria-label="pinterest">
                        <i class="ui-pinterest"></i>
                      </a>
                    </div>
                  </div>
                </div> <!-- share -->

                <div class="entry__article">
                  <p>iPrice Group report offers insights on <a href="#">daily e-commerce</a> activity in the Philippines and Southeast. Statistically, you stand a better chance for success if you have some sort of strategic ask in almost everything that you do -- in-person, on the phone, over email, or on social media.</p>

                  <p><strong>Think about it:</strong> If you make one additional ask per day and convert at around 10 percent. Then you have three people each month providing you with benefits that you'd have missed otherwise It's essential to make sure that your ask relates to some direct path to what you want, whether it is revenue, a business relationship or anything else of prime importance to you.</p>

                  <blockquote><p>“Dreams and dedication are powerful combination.”</p>
                  </blockquote>

                  <p>Music can help you get into a “flow state” -- losing yourself in the task at hand. Even repetitive tasks or mundane assignments seem more bearable, or even fun, when your favorite tunes are in your ears.</p>

                  <h2>Set a bigger goals and chase them everyday</h2>
                  <p>Plus, your eyes won’t be so prone to checking the time. <a href="#">Check out these</a> and more reasons to bring your music to work in this Zing Instruments infographic below. A great piece of music is an instant mood lifter. Plenty of scientific evidence backs this up - we`re happier bunnies when listening to music.</p>

                  <figure class="alignleft">
                    <img data-src="{{asset('media/img/content/single/single_post_img_1.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                    <figcaption>Having specific asks</figcaption>
                  </figure>

                  <p>Nulla rhoncus orci varius purus lobortis euismod. Fusce tincidunt dictum est et rhoncus. <strong>Vivamus hendrerit congue nisi, et nisl tincida</strong> vestibulum elit tincidunt eu. Vivamus ac pharetra orci, in feugiat massa. Proin congue mauris pretium, ultricies tortor in, aliquam urna. Vivamus mi tortor, <a href="#">finibus a interdum</a> ac, ultricies in elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere hendrerit ex eu scelerisque.</p>

                  <h4>Attraction needs attention</h4>
                  <p>In order to attract what you want, you actually have to consciously and strategically think about what you want and focus in on it. Then, you need to take some sort of action using the same <a href="#">four strategies</a> you use to ask for help in order to make it happen. You can't get what you want sitting around on your couch. You need to put yourself out there and stimulate interest in person, via email, by phone and through social media.</p>

                  <h2>Stylish article pages</h2>

                  <figure class="alignright">
                    <img data-src="{{asset('media/img/content/single/single_post_img_2.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                    <figcaption>make it happen</figcaption>
                  </figure>

                  <p>Stimulating interest in person can be powerful, especially if you surround yourself with the right people and the right ideas. A study by Nielsen found that <strong>83 percent of people trust referrals</strong> from others they know. If you have a product, service, content or any other value that you provide to others, let them know when you talk in person or over the phone. As an example, if you're working with a charity organization, <a href="#">tell a story</a> about how much money you helped raise for another charity you're affiliated with.</p>

                  <p>If you win an important award in an industry, put it in your email signature or as a tagline in a piece of social content. Showcasing your wins organically and authentically will attract more of the same.</p>

                  <h5>List of features</h5>
                  <ul>
                    <li>Reusable components</li>
                    <li>Multiple niches</li>
                    <li>Lightning fast</li>
                    <li>BEM methodology</li>
                    <li>Organized JS/Sass files</li>
                  </ul>

                  <h6>Summary</h6>

                  <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. And finally the subconscious is the mechanism through which thought impulses which are repeated regularly with feeling and emotion are quickened, charged. And finally the subconscious is the mechanism through which thought impulses which are repeated regularly with feeling and emotion.</p>

                  <!-- Final Review -->
                  <div class="final-review" style="background-image: url({{url('media/img/content/single/final_review.jpg')}})">
                    <div class="final-review__score">
                      <span class="final-review__score-number">9.2</span>
                    </div>
                    <div class="final-review__text-holder">
                      <h6 class="final-review__title">Great</h6>
                      <p class="final-review__text">Lovingly rendered real-world space tech,playing through actual missions is a special thrill,scoring system gives much needed additional incentive to perfect your designs</p>
                    </div>
                  </div> <!-- end final review -->

                  <!-- tags -->
                  <div class="entry__tags">
                    <i class="ui-tags"></i>
                    <span class="entry__tags-label">Tags:</span>
                    <a href="#" rel="tag">mobile</a><a href="#" rel="tag">gadgets</a><a href="#" rel="tag">satelite</a>
                  </div> <!-- end tags -->

                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->



            <!-- Comments -->
            <div class="entry-comments">
              <div class="title-wrap title-wrap--line">
                <h3 class="section-title">3 commentaires</h3>
              </div>
              <ul class="comment-list">
                <li class="comment">
                  <div class="comment-body">
                    <div class="comment-avatar">
                      <img alt="" src="img/content/single/comment_1.jpg">
                    </div>
                    <div class="comment-text">
                      <h6 class="comment-author">Joeby Ragpa</h6>
                      <div class="comment-metadata">
                        <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                      </div>
                      <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                      <a href="#" class="comment-reply">Reply</a>
                    </div>
                  </div>

                  <ul class="children">
                    <li class="comment">
                      <div class="comment-body">
                        <div class="comment-avatar">
                          <img alt="" src="img/content/single/comment_2.jpg">
                        </div>
                        <div class="comment-text">
                          <h6 class="comment-author">Alexander Samokhin</h6>
                          <div class="comment-metadata">
                            <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                          </div>
                          <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                          <a href="#" class="comment-reply">Reply</a>
                        </div>
                      </div>
                    </li> <!-- end reply comment -->
                  </ul>

                </li> <!-- end 1-2 comment -->

                <li>
                  <div class="comment-body">
                    <div class="comment-avatar">
                      <img alt="" src="img/content/single/comment_3.jpg">
                    </div>
                    <div class="comment-text">
                      <h6 class="comment-author">Chris Root</h6>
                      <div class="comment-metadata">
                        <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                      </div>
                      <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                      <a href="#" class="comment-reply">Reply</a>
                    </div>
                  </div>
                </li> <!-- end 3 comment -->

              </ul>
            </div> <!-- end comments -->

            <!-- Comment Form -->
            <div id="respond" class="comment-respond">
              <div class="title-wrap">
                <h5 class="comment-respond__title section-title">Leave a Reply</h5>
              </div>
              <form id="form" class="comment-form" method="post" action="#">
                <p class="comment-form-comment">
                  <label for="comment">Comment</label>
                  <textarea id="comment" name="comment" rows="5" required="required"></textarea>
                </p>

                <div class="row row-20">
                  <div class="col-lg-4">
                    <label for="name">Name: *</label>
                    <input name="name" id="name" type="text">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Email: *</label>
                    <input name="email" id="email" type="email">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Website:</label>
                    <input name="website" id="website" type="text">
                  </div>
                </div>

                <p class="comment-form-submit">
                  <input type="submit" class="btn btn-lg btn-color btn-button" value="Post Comment" id="submit-message">
                </p>

              </form>
            </div> <!-- end comment form -->

          </div> <!-- end content box -->
        </div> <!-- end post content -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Newsletter -->
          <aside class="widget widget_mc4wp_form_widget">
            <h4 class="widget-title">Newsletter</h4>
            <p class="newsletter__text">
              <i class="ui-email newsletter__icon"></i>
              Subscribe for our daily news
            </p>
            <form class="mc4wp-form" method="post">
              <div class="mc4wp-form-fields">
                <div class="form-group">
                  <input type="email" name="EMAIL" placeholder="Your email" required="">
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                </div>
              </div>
            </form>
          </aside> <!-- end widget newsletter -->

          <!-- Widget Socials -->
          <aside class="widget widget-socials text-center">
            <div class="socials socials--rounded socials--large">
              <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a><!--
              --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a><!--
              --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a><!--
              --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a><!--
              --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                <i class="ui-rss"></i>
              </a>
            </div>
          </aside> <!-- end widget socials -->


          <!-- Widget Ad 300 -->
          <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/placeholder_336.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

           <!-- Widget Ad 300 -->
           <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/placeholder_300_600.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

           <!-- Widget Ad 300 -->
           <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/carousel/Airtel_2015.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

           <!-- Widget Ad 300 -->
           <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/pub_rdcjobdays.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

          <!-- Widget Ad 300 -->
          <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/Airtel-woooh.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

           <!-- Widget Ad 300 -->
           <aside class="widget widget_media_image">
            <a href="#">
              <img src="{{asset('media/img/content/Pub-findin.jpg')}}" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->

        </aside> <!-- end sidebar -->

      </div> <!-- end content -->
    </div> <!-- end main container -->



    <!-- Newsletter Wide -->
    <div class="newsletter-wide">
      <div class="container">
        <div class="row justify-content-lg-center">
          <div class="col-xl-6 col-lg-8">
            <div class="widget widget_mc4wp_form_widget">
              <div class="newsletter-wide__container">
                <div class="newsletter-wide__text-holder">
                  <p class="newsletter-wide__text">
                    <i class="ui-email newsletter__icon"></i>
                    Subscribe for our daily news
                  </p>
                </div>
                <div class="newsletter-wide__form">
                  <form class="mc4wp-form" method="post">
                    <div class="mc4wp-form-fields">
                      <div class="form-group">
                        <input type="email" name="EMAIL" placeholder="Your email" required="">
                      </div>
                      <div class="form-group">
                        <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- end newsletter wide -->


    <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  @include('structure/linkbottom')

</body>
</html>