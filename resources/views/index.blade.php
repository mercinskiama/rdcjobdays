<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rdcjobdays | Accueil</title>
   <!--==========================
    Links
   ============================-->
  @include('structure/linkUp')
</head>

<body class="bg-light style-default style-rounded">

  <!-- Preloader -->
  @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="/about">About</a></li>
              <li><a href="/contact">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->

    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->

    <!-- Trending Now -->
    <div class="container">
      <div class="trending-now">
        <span class="trending-now__label">
          <i class="ui-flash"></i>
          <span class="trending-now__text d-lg-inline-block d-none">BREAKING NEWS</span>
        </span>
        <div class="newsticker">
          <ul class="newsticker__list">
            @foreach($breaknews as $news)
            <li class="newsticker__item"><a href="/Presente" style="font-weight:700;" class="newsticker__item-url">{{$news->titre_actu}}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="newsticker-buttons">
          <button class="newsticker-button newsticker-button--prev" id="newsticker-button--prev" aria-label="next article"><i class="ui-arrow-left"></i></button>
          <button class="newsticker-button newsticker-button--next" id="newsticker-button--next" aria-label="previous article"><i class="ui-arrow-right"></i></button>
        </div>
      </div>
    </div>

    <!-- Featured Posts Grid -->
    <section class="featured-posts-grid">
      <div class="container">
        <div class="row row-8">
          <div class="col-lg-6">
              <!-- Large post -->
              <div class="featured-posts-grid__item featured-posts-grid__item--lg">
                <article class="entry card featured-posts-grid__entry">
                    <div class="entry__img-holder card__img-holder">

                      <a href="/Presente">
                        <img src="{{asset('media/img/content/hero/hero_post_25.jpg')}}" alt="" class="entry__img">
                      </a>
                      <a href="/Presente" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--red">News</a>

                    </div>

                    <div class="entry__body card__body">

                      <h2 class="entry__title">
                        <a href="/Presente">Félix Tshisekedi annonce la nomination d’un ambassadeur plénipotentiaire de la RDC en Israël</a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <span>by</span>
                          <a href="#">erth</a>
                        </li>
                        <li class="entry__meta-date">
                        dfg
                        </li>
                      </ul>

                    </div>
                </article>
              </div>
              <!-- end large post -->
          </div>
           <!--  -->
          <div class="col-lg-6">
            <!-- Small post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">

                @foreach($economie as $eco)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$eco->image1)}});">
                  <a href="/Presente/{{$eco->id}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$eco->image1)}}" alt="" class="entry__img d-none">
                  <a href="/Presente/{{$eco->id}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">Economie</a>
                  <h6 class="thumb-entry-title-info">
                    <a href="/Presente/{{$eco->id}}">{{$eco->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach

                @foreach($securite as $secu)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$secu->image1)}});">
                  <a href="/Presente/{{$secu->id}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">Sécurité</a>
                  <a href="/Presente/{{$secu->id}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$secu->image1)}}" alt="" class="entry__img d-none">
                  <h6 class="thumb-entry-title-info">
                  <a href="/Presente/{{$secu->id}}">{{$secu->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach

              </article>
            </div>
            <!-- end post -->

            <!-- Small post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">

              @foreach($politique as $poli)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$poli->image1)}});">
                  <a href="/Presente/{{$poli->id}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$poli->image1)}}" alt="" class="entry__img d-none">
                  <a href="/Presente/{{$poli->id}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">Politique</a>
                  <h6 class="thumb-entry-title-info">
                  <a href="/Presente/{{$poli->id}}">{{$poli->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach

                @foreach($societe as $sol)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$sol->image1)}});">
                  <a href="/Presente/{{$sol->id}}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">Societé</a>
                  <a href="/Presente/{{$sol->id}}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$sol->image1)}}" alt="" class="entry__img d-none">
                  <h6 class="thumb-entry-title-info">
                  <a href="/Presente/{{$sol->id}}}">{{$sol->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach
              </article>
            </div> <!-- end post -->

            <!-- Small post -->

            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">
                @foreach($sport as $sp)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$sp->image1)}});">
                  <a href="/Presente/{{$sp->id}}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$sp->image1)}}" alt="" class="entry__img d-none">
                  <a href="/Presente/{{$sp->id}}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">Sport</a>
                  <h6 class="thumb-entry-title-info">
                  <a href="/Presente/{{$sp->id}}}">{{$sp->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach

                @foreach($culture as $sol)
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url({{url('media/img/content/hero/'.$sol->image1)}})">
                  <a href="/Presente/{{$sol->id}}}" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">Culture</a>
                  <a href="/Presente/{{$sol->id}}}" class="thumb-url"></a>
                  <img src="{{asset('media/img/content/hero/'.$sol->image1)}}" alt="" class="entry__img d-none">
                  <h6 class="thumb-entry-title-info">
                  <a href="/Presente/{{$sol->id}}}">{{$sol->titre_actu}}</a>
                  </h6>
                </div>
                @endforeach
              </article>
            </div>

            <!-- end post -->

          </div>
            <!-- end col -->


        </div>
      </div>
    </section>
    <!-- end featured posts grid -->

    <div class="main-container container pt-24" id="main-container">

      <!-- Carousel posts -->
       <section class="section mb-0">
        <div class="title-wrap title-wrap--line title-wrap--pr">
          <h3 class="section-title">Sponsors & Partenaires</h3>
        </div>

        <!-- Slider  -->
        <div id="owl-posts" class="owl-carousel owl-theme owl-carousel--arrows-outside">
          @foreach($partenaire as $part)
          <article class="entry thumb thumb--size-1">
            <div class="entry__img-holder thumb__img-holder" style="background-image: url({{asset('media/img/content/carousel/'.$part->image)}});">
              <div class="bottom-gradient"></div>
              <div class="thumb-text-holder">
                <h2 class="thumb-entry-title">
                  <a href="{{$part->link}}" target="_blank">{{$part->detail}}</a>
                </h2>
              </div>
              <a href="{{$part->link}}" target="_blank" class="thumb-url"></a>
            </div>
          </article>
          @endforeach
        </div>
        <!-- end slider -->

      </section>
      <!-- end carousel posts -->


      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content">

          <!-- Latest News -->
          <section class="section tab-post mb-16">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">OFFRES D'EMPOIS</h3>

              <div class="tabs tab-post__tabs">

                <ul class="tabs__list">
                  @foreach($cat_offre as $item)
                  <li class="tabs__item tabs__item--active">
                    <a href="#tab-all{{$item->id}}" class="tabs__trigger">{{$item->categorie}}</a>
                  </li>
                  @endforeach
                </ul> <!-- end tabs -->
              </div>
            </div>

            <!-- tab content -->
            <div class="tabs__content tabs__content-trigger tab-post__tabs-content">

              <div class="tabs__content-pane tabs__content-pane--active" id="tab-all1">
                <div class="row">
                  @foreach($offre_emploi as $item)
                    <div class="col-lg-6">
                      <article class="post-list-small__entry clearfix">
                          <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-80">
                                <a href="/offre">
                                  <img data-src="{{asset('media/img/content/offre/'.$item->image1)}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                                </a>
                              </div>
                          </div>
                          <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="/offre">{{$item->titre}}</a>
                              </h3>
                              <ul class="entry__meta">
                                <li class="entry__meta-author">
                                    <a href="#">{{$item->lieu}}</a>
                                </li>
                                <li class="entry__meta-date">
                                   {{\Carbon\Carbon::parse($item->date_publish)->format('d M Y')}}
                                </li>
                              </ul>
                          </div>
                      </article>
                    </div>
                  @endforeach

                </div>

              </div>
              <!-- end pane 1 -->

              <div class="tabs__content-pane tabs__content-pane--active" id="tab-all2">
                <div class="row">
                  @foreach($appel_offre as $item)
                    <div class="col-lg-6">
                      <!-- <ul class="post-list-small post-list-small--2 mb-32">/offre
                        <li class="post-list-small__item">
                            <article class="post-list-small__entry clearfix">
                              <div class="post-list-small__img-holder">
                                <div class="thumb-container thumb-80">
                                  <a href="single-post-games.html">
                                    <img data-src="{{asset('media/img/content/post_small/post_small_25.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                                  </a>
                                </div>
                              </div>
                              <div class="post-list-small__body">
                                <h3 class="post-list-small__entry-title">
                                  <a href="single-post-games.html">'Final Fantasy XV' DLC roadmap news: Plan for expansions to extend until 2019</a>
                                </h3>
                                <ul class="entry__meta">
                                  <li class="entry__meta-author">
                                    <span>by</span>
                                    <a href="#">DeoThemes</a>
                                  </li>
                                  <li class="entry__meta-date">
                                    Jan 21, 2018
                                  </li>
                                </ul>
                              </div>
                            </article>
                        </li>
                        <li class="post-list-small__item">
                            <article class="post-list-small__entry clearfix">
                              <div class="post-list-small__img-holder">
                                <div class="thumb-container thumb-70">
                                  <a href="single-post-politics.html">
                                    <img data-src="{{asset('media/img/content/post_small/post_small_14.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                  </a>
                                </div>
                              </div>
                              <div class="post-list-small__body">
                                <h3 class="post-list-small__entry-title">
                                  <a href="single-post-politics.html">5 Pieces of Hard-Won Wisdom From Billionaire Warren Buffett</a>
                                </h3>
                              </div>
                            </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_15.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Amnesty says global superpowers are 'callously undermining the rights of millions'</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_16.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Extreme Heat Waves Will Change How We Live. We’re Not Ready</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_17.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Liberals to outline long-term Indigenous housing plan in budge</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_17.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Liberals to outline long-term Indigenous housing plan in budge</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_17.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Liberals to outline long-term Indigenous housing plan in budge</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                        <li class="post-list-small__item">
                          <article class="post-list-small__entry clearfix">
                            <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-70">
                                <a href="single-post-politics.html">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_17.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                                </a>
                              </div>
                            </div>
                            <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="single-post-politics.html">Liberals to outline long-term Indigenous housing plan in budge</a>
                              </h3>
                            </div>
                          </article>
                        </li>
                      </ul> -->
                      <article class="post-list-small__entry clearfix">
                          <div class="post-list-small__img-holder">
                              <div class="thumb-container thumb-80">
                                <a href="/offre">
                                  <img data-src="{{asset('media/img/content/post_small/post_small_5.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                                </a>
                              </div>
                          </div>
                          <div class="post-list-small__body">
                              <h3 class="post-list-small__entry-title">
                                <a href="/offre">{{$item->titre}}</a>
                              </h3>
                              <ul class="entry__meta">
                                <li class="entry__meta-author">
                                    <a href="#">{{$item->lieu}}</a>
                                </li>
                                <li class="entry__meta-date">
                                {{\Carbon\Carbon::parse($item->date_publish)->format('d M Y')}}
                                </li>
                              </ul>
                          </div>
                      </article>
                    </div>
                  @endforeach
                </div>
                <!-- <div class="col-lg-6 text-right text-md-center">
                    <a href="#">
                      <img src=" {{asset('media/img/content/placeholder_300_600.jpg')}}" alt="">
                    </a>
                </div> -->
              </div>
              <!-- end pane 1 -->
              <div class="col-lg-6 text-right text-md-center">
                  <a href="#">
                    <img src=" {{asset('media/img/content/placeholder_300_600.jpg')}}" alt="">
                  </a>
              </div>

            </div>
            <!-- end tab content -->


          </section>
          <!-- end latest news -->

        </div>
        <!-- end posts -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">
            <!-- Widget Categories -->
            <aside class="widget widget_categories">
              <h4 class="widget-title">Domaine d'offre</h4>
              <ul>
                <li><a href="categories.html">Santé<span class="categories-count">5</span></a></li>
                <li><a href="categories.html">Ingénieurie <span class="categories-count">7</span></a></li>
                <li><a href="categories.html">Informatique<span class="categories-count">5</span></a></li>
                <li><a href="categories.html">Communication <span class="categories-count">8</span></a></li>
                <li><a href="categories.html">Administration <span class="categories-count">10</span></a></li>
                <li><a href="categories.html">Divers <span class="categories-count">18</span></a></li>
              </ul>
            </aside>

            <!-- end widget categories -->

            <!-- Widget Popular Posts   widget widget-popular-posts-->
            <aside class="">
              <article class="entry card">
                <div class="entry__img-holder card__img-holder">
                  <a href="single-post.html">
                    <div class="thumb-container thumb-70">
                      <img data-src="{{asset('media/img/content/placeholder_336.jpg')}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="" />
                    </div>
                  </a>
                  <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                </div>

                <div class="entry__body card__body">
                  <div class="entry__header">

                    <h2 class="entry__title">
                      <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                    </h2>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                      <li class="entry__meta-date">
                        Jan 21, 2018
                      </li>
                    </ul>
                  </div>
                  <div class="entry__excerpt">
                    <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                  </div>
                </div>
              </article>
            </aside>
            <!-- end widget popular posts -->

            <!-- Widget Newsletter -->
            <aside class="widget widget_mc4wp_form_widget">
              <h4 class="widget-title">Newsletter</h4>
              <p class="newsletter__text">
                <i class="ui-email newsletter__icon"></i>
                S'enregistrer pour recevoir des nouvelles
              </p>
              <form class="mc4wp-form" method="POST" action="/newsletter">
              {{csrf_field()}}
                <div class="mc4wp-form-fields">
                  <div class="form-group">
                    <input type="text" name="email" placeholder="Entrer votre adresse mail" required="">
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-color" value="Souscrire">
                  </div>
                </div>
              </form>
            </aside>
            <!-- end widget newsletter -->

        </aside>
        <!-- end sidebar -->

      </div>
      <!-- end content -->

      <!-- Ad Banner 728 -->
      <div class="text-center pb-48">
        <a href="#">
          <img src="{{asset('media/img/content/placeholder_728.jpg')}}" alt="">
        </a>
      </div>

    </div>
    <!-- end main container -->



    <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>
  </main>
  <!-- end main-wrapper -->
  <!-- Link Bottom -->
   @include('structure/linkbottom')
</body>
</html>