  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link href='https://fonts.googleapis.com/css?family=Lora:400i,700%7CBarlow:400,500,700' rel='stylesheet'>
  <!-- Css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/font-icons.css" />
  <link rel="stylesheet" href="css/style.css" /> -->
  <!-- <link rel="stylesheet" href="css/colors/pink.css" /> -->
  <!-- <link rel="stylesheet" href="css/colors/red.css" /> -->

  {{HTML::style('media/css/bootstrap.min.css')}}
  {{HTML::style('media/css/font-icons.css')}}
  {{HTML::style('media/css/style.css')}}

  <!-- {{HTML::style('media/css/colors/pink.css')}} -->
    <!-- {{HTML::style('media/css/colors/red.css')}} -->


  <!-- Favicons -->
  <!-- <link rel="shortcut icon" href="img/favicon.ico">
  <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png"> -->

  {{HTML::favicon('/media/img/favicon.ico')}}
  {{HTML::favicon('/media/img/apple-touch-icon.png')}}
  {{HTML::favicon('/media/img/apple-touch-icon-114x114.png')}}


  <!-- Lazyload (must be placed in head in order to work) -->
  <script src="js/lazysizes.min.js"></script>
  {{HTML::script('media/js/lazysizes.min.js')}}


  <!--//tags -->
  {{HTML::style('media/css/css/bootstrap.css')}}
  {{HTML::style('media/css/css/style.css')}}
  {{HTML::style('media/css/css/font-awesome.css')}}
