<!-- Sidenav -->
<header class="sidenav" id="sidenav">

<!-- close -->
<div class="sidenav__close">
  <button class="sidenav__close-button" id="sidenav__close-button" aria-label="close sidenav">
    <i class="ui-close sidenav__close-icon"></i>
  </button>
</div>

<!-- Nav -->
<nav class="sidenav__menu-container">
  <div class="row">
      <div class="col-lg-12" style="margin-bottom: 10%;">
          <a href="/" class="logo">
            <img class="logo__img" src="{{asset('media/img/logo.jpg')}}" srcset="{{asset('media/img/logo.jpg')}}, {{asset('img/logo.jpg')}}" alt="logo">
          </a>
      </div>

      <div class="col-lg-12">
          <ul class="sidenav__menu" role="menubar">
            <!-- <li>
              <a href="/" class="sidenav__menu-url">ACTUALITE</a>
            </li>  -->

            <li>
              <a href="/" class="sidenav__menu-url">ACCUEIL</a>
            </li>

            <li>
              <a href="/about" class="sidenav__menu-url">RDCJOBDAYS</a>
              <button class="sidenav__menu-toggle" aria-haspopup="true" aria-label="Open dropdown"><i class="ui-arrow-down"></i></button>
              <ul class="sidenav__menu-dropdown">
                <li><a href="/about" class="sidenav__menu-url">A Propos</a></li>
                <li><a href="/contact" class="sidenav__menu-url">Contact</a></li>
                <!-- <li><a href="/candidat" class="sidenav__menu-url">Candidats</a></li> -->
                <!-- <li><a href="categories.html" class="sidenav__menu-url">Employeurs</a></li> -->
                <li><a href="#" class="sidenav__menu-url">Partenaire</a></li>
                <!-- <li><a href="404.html" class="sidenav__menu-url">404</a></li> -->
              </ul>
            </li>
            <li>
                <a href="/offre" class="sidenav__menu-url">OFFRES D'EMPLOI</a>
            </li>

            <!-- Categories -->
            <li>
              <a href="#" class="sidenav__menu-url">CV ONLINE</a>
            </li>

            <li>
              <a href="#" class="sidenav__menu-url">FINDING</a>
            </li>
          </ul>
      </div>
  </div>
</nav>

<div class="socials sidenav__socials">
  <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
    <i class="ui-facebook"></i>
  </a>
  <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
    <i class="ui-twitter"></i>
  </a>
  <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
    <i class="ui-google"></i>
  </a>
  <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
    <i class="ui-youtube"></i>
  </a>
  <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
    <i class="ui-instagram"></i>
  </a>
</div>
</header>
<!-- end sidenav -->