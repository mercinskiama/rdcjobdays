<!DOCTYPE html>
<html lang="en">
<head>
    <title>Rdcjobdays | offre</title>
   <!--==========================
    Links
   ============================-->
  @include('structure/linkUp')
</head>

<body class="bg-light style-default style-rounded">

  <!-- Preloader -->
  @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="#">About</a></li>
              <li><a href="#">Advertise</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->

    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="/" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Offre d'emploi
        </li>

      </ul>
    </div>


    <div class="main-container container" id="main-container">
        <!-- /inner_content -->
            <div class="inner_content_info_agileits">
              <div class="container">
                <div class="tittle_head_w3ls">
                  <h3 class="tittle">EMPLOI DISPONIBLE</h3>
                </div>
                <div class="inner_sec_grids_info_w3ls">
                  <div class="col-md-8 job_info_left">
                    <div class="but_list">
                      <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="/offre" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Offre d'emploi</a></li>
                          <!-- <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Show Descriptions</a></li> -->
                        </ul>
                        <div id="myTabContent" class="tab-content">
                        @if(count($offre_emploi) == 0)
                          <div class="">
                            <h2>Aucune offre d'emploi de ce type n'est enregistré pour l'instant...</h2>
                          </div>
                        @else
                          <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                            @foreach($offre_emploi as $item)
                              <div class="tab_grid">
                                <div class="col-sm-3 loc_1">
                                  <a href="#"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                  <div class="jobs_right">
                                    <div class="date">{{\Carbon\Carbon::parse($item->date_publish)->format('d') }}<span>{{\Carbon\Carbon::parse($item->date_publish)->format('M')}}</span></div>
                                    <div class="date_desc">
                                      <h6 class="title"><a href="#">{{$item->titre}}</a></h6>
                                      <span style="font-weight:700">Lieu :</span><span class="meta">{{$item->lieu}}</span>
                                    </div>
                                    <p class="description">
                                        <span style="font-weight:800;text-transform: capitalize;">Entreprise:</span>
                                        <span> {{$item->entreprise}} </span>
                                    </p>
                                    <div class="read">
                                      @if($item->etat == 'encours')
                                        <i class="fa fa-circle" style="color:#13CA07;"></i> en cours
                                      @elseif($item->etat == 'expire')
                                       <i class="fa fa-circle" style="color:#AF493F;"></i>  expiré
                                      @elseif($item->etat == 'non precise')
                                       <i class="fa fa-circle" style="color:#5D5D5D;"></i>  non precisé
                                      @endif
                                      &nbsp; &nbsp; &nbsp;
                                      <a href="#" class="read-more">Plus &nbsp;<i class="fa fa-caret-right icon_1" style="color:#fff;"></i> </a>
                                    </div>

                                    <ul class="top-btns">
                                      <li><a href="#" class="fa fa-calendar toggle"></a></li>
                                      <li>{{\Carbon\Carbon::parse($item->date_fin)->format('d M Y')}}</li>
                                    </ul>
                                    <div class="clearfix"> </div>
                                  </div>
                                </div>
                                <div class="clearfix"> </div>
                              </div>
                              <hr>
                            @endforeach
                            <!-- <div class="tab_grid">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">23 <span>Nov</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">01 <span>Dec</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">13 <span>Dec</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">14 <span>Nov</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">20 <span>Dec</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid last">
                              <div class="col-sm-3 loc_1">
                                <a href="location_single.html"><img src="{{asset('media/img/content/post_small/post_small_22.jpg')}}" alt=""></a>
                              </div>
                              <div class="col-sm-9">
                                <div class="jobs_right">
                                  <div class="date">30 <span>Dec</span></div>
                                  <div class="date_desc">
                                    <h6 class="title"><a href="single.html">Front-end Developer</a></h6>
                                    <span class="meta">Franisco Da Silva</span>
                                  </div>

                                  <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus
                                    similique aliquid a dolores autem laudantium sapiente.</p>
                                  <div class="read"><a href="single.html" class="read-more">Read More</a></div>

                                  <ul class="top-btns">
                                    <li><a href="#" class="fa fa-plus toggle"></a></li>
                                    <li><a href="#" class="fa fa-star"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                  </ul>
                                  <div class="clearfix"> </div>
                                </div>
                              </div>
                              <div class="clearfix"> </div>
                            </div> -->
                          </div>
                          <div class="d-flex justify-content-center">
                            {{ $offre_emploi->links() }}
                          </div>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 job_info_right">

                  <div class="widget_search">
                    <h5 class="widget-title">Recherche offre</h5>
                    <form method="POST" action="/search">
                      {{csrf_field()}}
                      <div class="widget-content">
                        <span>je cherche l'offre de...</span>
                        <select class="form-control jb_1" name="categorie" id="categorie">
                          <option value="0">Categorie</option>
                          <option value="appel d\'offre">Appel d'offre</option>
                          <option value="offre d\'emploi">Offre d'emploi</option>
                        </select>
                        <span>A</span>
                        <select class="form-control jb_1" name="ville" id="ville">
                          <option value="0">Ville</option>
                          @foreach($ville as $item)
                          <option value="{{$item->lieu}}">{{$item->lieu}}</option>
                          @endforeach
                        </select>

                        <select class="form-control jb_1" name="ville" id="ville">
                          <option value="0">Domaine</option>
                          @foreach($domaine as $item)
                          <option value="{{$item->domaine}}">{{$item->demaine}}</option>
                          @endforeach
                        </select>

                        <!-- <input type="text" class="form-control jb_2" id="domaine" name="domaine" placeholder="Domaine"> -->
                        <input type="submit" value="Rechercher">
                      </div>
                    </form>
                  </div>
                  <div class="col_3 permit">
                    <!-- Widget Categories -->
                    <aside class="widget widget_categories">
                      <h4 class="widget-title">Domaine d'offre</h4>
                      <ul>
                          <li><a href="categories.html">Santé<span class="categories-count">5</span></a></li>
                          <li><a href="categories.html">Ingénieurie <span class="categories-count">7</span></a></li>
                          <li><a href="categories.html">Informatique<span class="categories-count">5</span></a></li>
                          <li><a href="categories.html">Communication <span class="categories-count">8</span></a></li>
                          <li><a href="categories.html">Administration <span class="categories-count">10</span></a></li>
                          <li><a href="categories.html">Divers <span class="categories-count">18</span></a></li>
                      </ul>
                    </aside>
                  </div>
                  <div class="col_3 ">
                        <!-- Widget Popular Posts   widget widget-popular-posts-->
                        <aside class="">
                          <article class="entry card">
                            <div class="entry__img-holder card__img-holder">
                              <a href="single-post.html">
                                <div class="thumb-container thumb-70">
                                  <img data-src="{{asset('media/img/content/placeholder_336.jpg')}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="" />
                                </div>
                              </a>
                              <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                            </div>

                            <div class="entry__body card__body">
                              <div class="entry__header">

                                <h2 class="entry__title">
                                  <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                                </h2>
                                <ul class="entry__meta">
                                  <li class="entry__meta-author">
                                    <span>by</span>
                                    <a href="#">DeoThemes</a>
                                  </li>
                                  <li class="entry__meta-date">
                                    Jan 21, 2018
                                  </li>
                                </ul>
                              </div>
                              <div class="entry__excerpt">
                                <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                              </div>
                            </div>
                          </article>
                        </aside>
                        <!-- end widget popular posts -->
                  </div>
                  <div class="col_3 permit">
                    <!-- Widget Newsletter -->
                    <!-- Widget Ad 300 -->
                    <aside class="widget widget_media_image">
                      <a href="#">
                        <img src="{{asset('media/img/content/Airtel-woooh.jpg')}}" alt="">
                      </a>
                    </aside> <!-- end widget ad 300 -->
                    <!-- end widget newsletter -->
                  </div>

                </div>
                <div class="clearfix"></div>
              </div>
            </div>
        <!-- //inner_content -->
    </div>
    <!-- end main container -->

    <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')

</body>
</html>