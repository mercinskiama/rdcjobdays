<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rdcjobdays | Contact</title>

  <!--==========================
    Links
   ============================-->
   @include('structure/linkUp')

</head>

<body class="style-default style-rounded">

   <!-- Preloader -->
   @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="#">About</a></li>
              <li><a href="#">Advertise</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->


     <!-- Navigation -->
     @include('structure/headerside')
    <!-- Navigation -->

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="/" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Contact
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">
      <!-- post content -->
      <div class="blog__content mb-72">
        <h1 class="page-title">Nous Contacter </h1>

        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h4>Envoyez nous  un Message</h4>
            <p>Veuillez utilisé le formulaire en bas pour nous laisser votre message.</p>
            <ul class="contact-items">
              <li class="contact-item"><address>372 Avenue Colonel Mondjiba Complexe Utex, Gombe, Kinshasa, RDC </address></li>
              <li class="contact-item"><a href="tel:+1-800-1554-456-123">+ 243 82 420 20 32</a></li>
              <li class="contact-item"><a href="mailto:themesupport@gmail.com">info@rdcjobdays.com</a></li>
            </ul>

            <!-- Contact Form -->
            <form id="contact-form" class="contact-form mt-30 mb-30" method="post" action="#">
              <div class="contact-name">
                <label for="name">Prenom - Nom <abbr title="required" style="color:#FF0000;" class="required">*</abbr></label>
                <input name="name" id="name" type="text">
              </div>
              <div class="contact-email">
                <label for="email">Email <abbr title="required" style="color:#FF0000;" class="required">*</abbr></label>
                <input name="email" id="email" type="email">
              </div>
              <div class="contact-subject">
                <label for="email">Sujet</label>
                <input name="subject" id="subject" type="text">
              </div>
              <div class="contact-message">
                <label for="message">Message <abbr title="required" style="color:#FF0000;" class="required">*</abbr></label>
                <textarea id="message" name="message" rows="7" required="required"></textarea>
              </div>
              <br> <br>
              <input type="submit" class="btn btn-lg btn-color btn-button" value="Envoyer" id="submit-message">

              <div id="msg" class="message"></div>
            </form>

          </div>
        </div>
      </div> <!-- end post content -->
    </div> <!-- end main container -->

   <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')


</body>
</html>