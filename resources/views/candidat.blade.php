<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rdcjobdays | Candidats</title>

  <!--==========================
    Links
   ============================-->
   @include('structure/linkUp')

</head>

<body class="style-default style-rounded">

   <!-- Preloader -->
   @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="#">About</a></li>
              <li><a href="#">Advertise</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->


     <!-- Navigation -->
     @include('structure/headerside')
    <!-- Navigation -->

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="index.html" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Candidats
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">
            <!-- /inner_content -->
            <div class="inner_content_info_agileits">
                <div class="container">
                    <div class="tittle_head_w3ls">
                        <h3 class="tittle">Candidates </h3>
                    </div>
                    <div class="inner_sec_grids_info_w3ls">
                        <div class="col-md-8 job_info_left">
                            <div class="tab_grid_prof">
                                <div class="col-sm-3 loc_1">
                                    <a href="#"><img src="{{asset('media/img/content/about/jeune1.jfif')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>Rodolphe Nsimba</h6>
                                        <span class="m_2_prof">Informaticien Developper web Mobile</span>
                                        <div class="person-info">
                                            <ul>
                                                <li><span>Statu</span>: Libre </li>
                                                <li><span>Genre</span>: Homme</li>
                                                <li><span>Location</span>: Kinshasa, 150, Lowa/Kinshasa</li>
                                                <li><span>Experience</span>: 3 ans</li>
                                                <li><span>Nationalité</span>: Congolaise</li>
                                            </ul>
                                        </div>
                                        <div class="read"><a href="#" class="read-more"> Plus</a></div>
                                        <ul class="top-btns">
                                            <li><a href="#" class="fa fa-plus toggle"></a></li>
                                            <li><a href="#" class="fa fa-star"></a></li>
                                            <li><a href="#" class="fa fa-link"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid_prof">
                                <div class="col-sm-3 loc_1">
                                    <a href="#"><img src="{{asset('media/img/content/about/jeune2.jpg')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>Francisco Kasongo</h6>
                                        <span class="m_2_prof">Specialisation en Fiscalité</span>
                                        <div class="person-info">
                                            <ul>
                                                <li><span>Statu</span>: Libre </li>
                                                <li><span>Genre</span>: Homme</li>
                                                <li><span>Location</span>: Kinshasa, 14b, Kongolo/Ngalima</li>
                                                <li><span>Experience</span>: 2 ans</li>
                                                <li><span>Nationalité</span>: Congolaise</li>
                                            </ul>
                                        </div>
                                        <div class="read"><a href="#" class="read-more">Plus</a></div>
                                        <ul class="top-btns">
                                            <li><a href="#" class="fa fa-plus toggle"></a></li>
                                            <li><a href="#" class="fa fa-star"></a></li>
                                            <li><a href="#" class="fa fa-link"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid_prof">
                                <div class="col-sm-3 loc_1">
                                    <a href="#"><img src="{{asset('media/img/content/about/jeune3.jfif')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>Gladis Matanda</h6>
                                        <span class="m_2_prof">management stratégique</span>
                                        <div class="person-info">
                                            <ul>
                                                <li><span>Statu</span>: Stagiaire</li>
                                                <li><span>Genre</span>: Homme</li>
                                                <li><span>Location</span>: Kinshasa, 23, Mbata/Lemba</li>
                                                <li><span>Experience</span>: 3 ans</li>
                                                <li><span>Nationalité</span>: Congolaise</li>
                                            </ul>
                                        </div>
                                        <div class="read"><a href="#" class="read-more">Plus</a></div>
                                        <ul class="top-btns">
                                            <li><a href="#" class="fa fa-plus toggle"></a></li>
                                            <li><a href="#" class="fa fa-star"></a></li>
                                            <li><a href="#" class="fa fa-link"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="tab_grid_prof">
                                <div class="col-sm-3 loc_1">
                                    <a href="#"><img src="{{asset('media/img/content/about/jeune4.jfif')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>Josué Dikoma</h6>
                                        <span class="m_2_prof">Medecin</span>
                                        <div class="person-info">
                                            <ul>
                                                <li><span>Statu</span>: Libre</li>
                                                <li><span>Genre</span>: Homme</li>
                                                <li><span>Location</span>: Lubumbashi, 23, Nseka/Malemba</li>
                                                <li><span>Experience</span>: 2 ans</li>
                                                <li><span>Nationalité</span>: Congolaise</li>
                                            </ul>
                                        </div>
                                        <div class="read"><a href="#" class="read-more">Plus</a></div>
                                        <ul class="top-btns">
                                            <li><a href="#" class="fa fa-plus toggle"></a></li>
                                            <li><a href="#" class="fa fa-star"></a></li>
                                            <li><a href="#" class="fa fa-link"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="clearfix"> </div>

                            </div>
                            <div class="tab_grid_prof">
                                <div class="col-sm-3 loc_1">
                                    <a href="#"><img src="{{asset('media/img/content/about/jeune5.jfif')}}" alt=""></a>
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>Joel Tshilumba</h6>
                                        <span class="m_2_prof">Ingénieur civil</span>
                                        <div class="person-info">
                                            <ul>
                                                <li><span>Statu</span>: libre</li>
                                                <li><span>Genre</span>: Homme</li>
                                                <li><span>Location</span>: Kongo Central, 19b, Mweka/Matadi</li>
                                                <li><span>Experience</span>: 2 ans</li>
                                                <li><span>Nationalité</span>: Congolaise</li>
                                            </ul>
                                        </div>
                                        <div class="read"><a href="#" class="read-more">Plus</a></div>
                                        <ul class="top-btns">
                                            <li><a href="#" class="fa fa-plus toggle"></a></li>
                                            <li><a href="#" class="fa fa-star"></a></li>
                                            <li><a href="#" class="fa fa-link"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <ul class="pagination paging w3-agileits-paging">
                                <li>
                                    <a href="#" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                            </a>
                                </li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                    <a href="#" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 job_info_right">

                            <div class="widget_search">
                                <h5 class="widget-title">Ajouter un candidat</h5>
                                <div class="widget-content">
                                    <input type="submit" value="Inserer">
                                </div>
                            </div>
                            <div class="col_3 permit">
                                <!-- Widget Categories -->
                                    <aside class="widget widget_categories">
                                    <h4 class="widget-title">Categorie de Candidats</h4>
                                    <ul>
                                        <li><a href="categories.html">Santé<span class="categories-count">5</span></a></li>
                                        <li><a href="categories.html">Ingénieurie <span class="categories-count">7</span></a></li>
                                        <li><a href="categories.html">Informatique<span class="categories-count">5</span></a></li>
                                        <li><a href="categories.html">Communication <span class="categories-count">8</span></a></li>
                                        <li><a href="categories.html">Administration <span class="categories-count">10</span></a></li>
                                        <li><a href="categories.html">Divers <span class="categories-count">18</span></a></li>
                                    </ul>
                                    </aside>
                            </div>
                            <div class="col_3">
                                <!-- Widget Popular Posts   widget widget-popular-posts-->
                                <aside class="">
                                    <article class="entry card">
                                        <div class="entry__img-holder card__img-holder">
                                        <a href="single-post.html">
                                            <div class="thumb-container thumb-70">
                                            <img data-src="{{asset('media/img/content/placeholder_336.jpg')}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="" />
                                            </div>
                                        </a>
                                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                                        </div>

                                        <div class="entry__body card__body">
                                        <div class="entry__header">

                                            <h2 class="entry__title">
                                            <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                                            </h2>
                                            <ul class="entry__meta">
                                            <li class="entry__meta-author">
                                                <span>by</span>
                                                <a href="#">DeoThemes</a>
                                            </li>
                                            <li class="entry__meta-date">
                                                Jan 21, 2018
                                            </li>
                                            </ul>
                                        </div>
                                        <div class="entry__excerpt">
                                            <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                        </div>
                                        </div>
                                    </article>
                                </aside>
                                <!-- end widget popular posts -->
                            </div>
                            <div class="col_3 permit">
                                <h3>Work Permit</h3>
                                    <!-- Widget Ad 300 -->
                                    <aside class="widget widget_media_image">
                                        <a href="#">
                                        <img src="{{asset('media/img/content/Airtel-woooh.jpg')}}" alt="">
                                        </a>
                                    </aside>
                                    <!-- end widget ad 300 -->
                            </div>
                            <div class="col_3 permit">
                                    <!-- Widget Ad 300 -->
                                    <aside class="widget widget_media_image">
                                        <a href="#">
                                        <img src="{{asset('media/img/content/Pub-findin.jpg')}}" alt="">
                                        </a>
                                    </aside> <!-- end widget ad 300 -->
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- //inner_content -->
    </div> <!-- end main container -->

   <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')


</body>
</html>