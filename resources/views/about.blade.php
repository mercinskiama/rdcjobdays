<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rdcjobdays | A propos</title>
  <!--==========================
    Links
   ============================-->
   @include('structure/linkUp')

</head>

<body class="style-default style-rounded">

  <!-- Preloader -->
  @include('structure/Preloader')


  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

   <!-- Sidenav -->
   @include('structure/header')
  <!-- end sidenav -->


  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="#">About</a></li>
              <li><a href="#">Advertise</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->


    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->


    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="/" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          A propos
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">
      <!-- post content -->
      <div class="blog__content mb-72">
        <h1 class="page-title">A Propos</h1>
        <img src="{{asset('media/img/content/about/about_bg.jpg')}}" class="page-featured-img">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="entry__article">
              <p>RDC JOB DAYS,  offers insights on daily e-commerce activity in the Philippines and Southeast. Statistically, you stand a better chance for success if you have some sort of strategic ask in almost everything that you do -- in-person, on the phone, over email, or on social media.</p>

              <p>Think about it: If you make one additional ask per day and convert at around 10 percent. Then you have three people each month providing you with benefits that you'd have missed otherwise It's essential to make sure that your ask relates to some direct path to what you want, whether it is revenue, a business relationship or anything else of prime importance to you.</p>

              <blockquote><p>“Dreams and dedication are powerful combination.”</p></blockquote>

              <p>This strategy uses the Ben Franklin Effect: When people do you a favor, they are more likely to do another. When you meet someone you yourself might be able to assist, ask for their help and, at the same time (e.g. in the same conversation) offer yours. And make a point to be of service even if others might not be able to help you immediately.</p>

              <p>The same goes for phone calls, emails or social media interactions. Being afraid to ask for what you want is a detriment to your success and prosperity. If you lack this skill, then you need to work on it! Asking for help is a form of radical humility, something that with practice will have amazing benefits for all involved.Use this study to your advantage! Don't just ask people for help - ask specifically for what you.</p>

              <p>In order to attract what you want, you actually have to consciously and strategically think about what you want and focus in on it. Then, you need to take some sort of action using the same four strategies you use to ask for help in order to make it happen. You can't get what you want sitting around on your couch. You need to put yourself out there and stimulate interest in person, via email, by phone and through social media.</p>

            </div>
          </div>
        </div>
      </div> <!-- end post content -->
    </div> <!-- end main container -->

    <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')

</body>
</html>