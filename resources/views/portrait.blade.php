<!DOCTYPE html>
<html lang="en">
<head>
  <title>Deus | Portrait</title>

  <!--==========================
    Links
   ============================-->
   @include('structure/linkUp')

</head>

<body class="bg-light single-post style-default style-rounded">

  <!-- Preloader -->
  @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->


  <main class="main oh" id="main">

    <!-- Top Bar -->
    <div class="top-bar d-none d-lg-block">
      <div class="container">
        <div class="row">

          <!-- Top menu -->
          <div class="col-lg-6">
            <ul class="top-menu">
              <li><a href="#">About</a></li>
              <li><a href="#">Advertise</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>

          <!-- Socials -->
          <div class="col-lg-6">
            <div class="socials nav__socials socials--nobase socials--white justify-content-end">
              <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                <i class="ui-facebook"></i>
              </a>
              <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                <i class="ui-twitter"></i>
              </a>
              <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                <i class="ui-google"></i>
              </a>
              <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                <i class="ui-youtube"></i>
              </a>
              <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                <i class="ui-instagram"></i>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- end top bar -->


    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->

   <!-- Breadcrumbs -->
   <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="/" class="breadcrumbs__url">Accueil</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Portrait
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">

      <!-- Content -->
      <div class="row">

        <!-- post content -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box">

            <!-- standard post -->
            <article class="entry mb-0">

              <div class="single-post__entry-header entry__header">
                <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--green">Portrait</a>
                <h1 class="single-post__entry-title">
                  {{$infoportrait->Titre}}
                </h1>

                <div class="entry__meta-holder">
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>Publié par</span>
                      <a href="#">RdcJobdays</a>
                    </li>
                    <li class="entry__meta-date">
                    {{\Carbon\Carbon::parse($infoportrait->created_at)->format('d M Y')}}
                    </li>
                  </ul>

                  <ul class="entry__meta">
                    <li class="entry__meta-views">
                      <i class="ui-eye"></i>
                      <span>1356</span>
                    </li>
                    <li class="entry__meta-comments">
                      <a href="#">
                        <i class="ui-chat-empty"></i>13
                      </a>
                    </li>
                  </ul>
                </div>
              </div> <!-- end entry header -->

              <div class="entry__img-holder">
                <img src="{{asset('media/img/content/carousel/'.$infoportrait->image1)}}" alt="" class="entry__img">
              </div>

              <div class="entry__article-wrap">

                <!-- Share -->
                <div class="entry__share">
                  <div class="sticky-col">
                    <div class="socials socials--rounded socials--large"> <!--target="_blank"-->
                      <a class="social social-facebook" href="#" title="facebook"  aria-label="facebook">
                        <i class="ui-facebook"></i>
                      </a>
                      <a class="social social-twitter" href="#" title="twitter"  aria-label="twitter">
                        <i class="ui-twitter"></i>
                      </a>
                      <a class="social social-google-plus" href="#" title="google"  aria-label="google">
                        <i class="ui-google"></i>
                      </a>
                      <a class="social social-pinterest" href="#" title="pinterest"  aria-label="pinterest">
                        <i class="ui-pinterest"></i>
                      </a>
                    </div>
                  </div>
                </div> <!-- share -->

                <div class="entry__article">
                  <p>
                       <strong>{{$infoportrait->prenom}} &nbsp; {{$infoportrait->nom}} &nbsp; {{$infoportrait->postnom}} </strong>
                       <br>
                       Né(ée) à {{$infoportrait->residence}}, &nbsp; le {{\Carbon\Carbon::parse($infoportrait->date_naissance)->format('d M Y')}}
                  </p>

                  <p><strong>Think about it:</strong> If you make one additional ask per day and convert at around 10 percent. Then you have three people each month providing you with benefits that you'd have missed otherwise It's essential to make sure that your ask relates to some direct path to what you want, whether it is revenue, a business relationship or anything else of prime importance to you.</p>

                  <blockquote><p>“Dreams and dedication are powerful combination.”</p>
                  </blockquote>

                  <p>Music can help you get into a “flow state” -- losing yourself in the task at hand. Even repetitive tasks or mundane assignments seem more bearable, or even fun, when your favorite tunes are in your ears.</p>

                  <h2>Set a bigger goals and chase them everyday</h2>
                  <p>Plus, your eyes won’t be so prone to checking the time. <a href="#">Check out these</a> and more reasons to bring your music to work in this Zing Instruments infographic below. A great piece of music is an instant mood lifter. Plenty of scientific evidence backs this up - we`re happier bunnies when listening to music.</p>

                  <figure class="alignleft">
                    <img data-src="{{asset('media/img/content/single/single_post_img_1.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                    <figcaption>Having specific asks</figcaption>
                  </figure>

                  <p>Nulla rhoncus orci varius purus lobortis euismod. Fusce tincidunt dictum est et rhoncus. <strong>Vivamus hendrerit congue nisi, et nisl tincida</strong> vestibulum elit tincidunt eu. Vivamus ac pharetra orci, in feugiat massa. Proin congue mauris pretium, ultricies tortor in, aliquam urna. Vivamus mi tortor, <a href="#">finibus a interdum</a> ac, ultricies in elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere hendrerit ex eu scelerisque.</p>

                  <h4>Attraction needs attention</h4>
                  <p>In order to attract what you want, you actually have to consciously and strategically think about what you want and focus in on it. Then, you need to take some sort of action using the same <a href="#">four strategies</a> you use to ask for help in order to make it happen. You can't get what you want sitting around on your couch. You need to put yourself out there and stimulate interest in person, via email, by phone and through social media.</p>

                  <h2>Stylish article pages</h2>

                  <figure class="alignright">
                    <img data-src="{{asset('media/img/content/single/single_post_img_2.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class="lazyload">
                    <figcaption>make it happen</figcaption>
                  </figure>

                  <p>Stimulating interest in person can be powerful, especially if you surround yourself with the right people and the right ideas. A study by Nielsen found that <strong>83 percent of people trust referrals</strong> from others they know. If you have a product, service, content or any other value that you provide to others, let them know when you talk in person or over the phone. As an example, if you're working with a charity organization, <a href="#">tell a story</a> about how much money you helped raise for another charity you're affiliated with.</p>

                  <p>If you win an important award in an industry, put it in your email signature or as a tagline in a piece of social content. Showcasing your wins organically and authentically will attract more of the same.</p>

                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->

              <!-- Related Posts -->
              <section class="section related-posts mt-40 mb-0">
                <div class="title-wrap title-wrap--line title-wrap--pr">
                  <h3 class="section-title">Recents Portraits</h3>
                </div>

                <!-- Slider -->
                <div id="owl-posts-3-items" class="owl-carousel owl-theme owl-carousel--arrows-outside">
                  @foreach($portraits as $item)
                  <article class="entry thumb thumb--size-1">
                    <div class="entry__img-holder thumb__img-holder" style="background-image: url({{url('media/img/content/carousel/'.$item->image1)}});">
                      <div class="bottom-gradient"></div>
                      <div class="thumb-text-holder">
                        <h2 class="thumb-entry-title">
                          <a href="/portrait/{{$item->id}}">{{$item->prenom}} &nbsp; {{$item->nom}} &nbsp; {{$item->postnom}}</a>
                        </h2>
                      </div>
                      <a href="/portrait/{{$item->id}}" class="thumb-url"></a>
                    </div>
                  </article>
                  @endforeach
                </div>
                <!-- end slider -->

              </section> <!-- end related posts -->

            </article>
            <!-- end standard post -->

            <!-- Comments -->
            <div class="entry-comments">
              <div class="title-wrap title-wrap--line">
                <h3 class="section-title">3 commentaires</h3>
              </div>
              <ul class="comment-list">
                <li class="comment">
                  <div class="comment-body">
                    <div class="comment-avatar">
                      <img alt="" src="{{asset('media/img/content/single/comment_1.jpg')}}">
                    </div>
                    <div class="comment-text">
                      <h6 class="comment-author">Joeby Ragpa</h6>
                      <div class="comment-metadata">
                        <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                      </div>
                      <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                      <a href="#" class="comment-reply">Reply</a>
                    </div>
                  </div>

                  <ul class="children">
                    <li class="comment">
                      <div class="comment-body">
                        <div class="comment-avatar">
                          <img alt="" src="{{asset('media/img/content/single/comment_2.jpg')}}">
                        </div>
                        <div class="comment-text">
                          <h6 class="comment-author">Alexander Samokhin</h6>
                          <div class="comment-metadata">
                            <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                          </div>
                          <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                          <a href="#" class="comment-reply">Reply</a>
                        </div>
                      </div>
                    </li> <!-- end reply comment -->
                  </ul>

                </li> <!-- end 1-2 comment -->

                <li>
                  <div class="comment-body">
                    <div class="comment-avatar">
                      <img alt="" src="{{asset('media/img/content/single/comment_3.jpg')}}">
                    </div>
                    <div class="comment-text">
                      <h6 class="comment-author">Chris Root</h6>
                      <div class="comment-metadata">
                        <a href="#" class="comment-date">July 17, 2017 at 12:48 pm</a>
                      </div>
                      <p>This template is so awesome. I didn’t expect so many features inside. E-commerce pages are very useful, you can launch your online store in few seconds. I will rate 5 stars.</p>
                      <a href="#" class="comment-reply">Reply</a>
                    </div>
                  </div>
                </li> <!-- end 3 comment -->

              </ul>
            </div>
            <!-- end comments -->

            <!-- Comment Form -->
            <div id="respond" class="comment-respond">
              <div class="title-wrap">
                <h5 class="comment-respond__title section-title">Leave a Reply</h5>
              </div>
              <form id="form" class="comment-form" method="post" action="#">
                <p class="comment-form-comment">
                  <label for="comment">Comment</label>
                  <textarea id="comment" name="comment" rows="5" required="required"></textarea>
                </p>

                <div class="row row-20">
                  <div class="col-lg-4">
                    <label for="name">Name: *</label>
                    <input name="name" id="name" type="text">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Email: *</label>
                    <input name="email" id="email" type="email">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Website:</label>
                    <input name="website" id="website" type="text">
                  </div>
                </div>

                <p class="comment-form-submit">
                  <input type="submit" class="btn btn-lg btn-color btn-button" value="Post Comment" id="submit-message">
                </p>

              </form>
            </div>
            <!-- end comment form -->

          </div> <!-- end content box -->
        </div> <!-- end post content -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Recentes Actualites</h4>
            <ul class="post-list-small">
              @foreach($recentActu as $recent)
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="/Presente/{{$recent->id}}">
                        <img data-src="{{asset('media/img/content/hero/'.$recent->image1)}}" src="{{asset('media/img/empty.png')}}" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="/Presente/{{$recent->id}}">{{$recent->titre_actu}}</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>By</span>
                        <a href="/Presente/{{$recent->id}}">{{$recent->auteur}}</a>
                      </li>
                      <li class="entry__meta-date">
                        {{\Carbon\Carbon::parse($recent->date_actu)->format('d M Y')}}
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
              @endforeach
            </ul>
            <div class="d-flex justify-content-center">
              {{ $recentActu->links() }}
            </div>
          </aside>
          <!-- end widget popular posts -->

          <!--  -->
          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Recents Portraits</h4>
            <ul class="post-list-small">
              @foreach($LessPortrait as $item)
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="/portrait/{{$item->id}}">
                        <img data-src="{{asset('media/img/content/carousel/'.$item->image1)}}" src="{{asset('media/img/empty.png')}}" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="/portrait/{{$item->id}}">{{$item->Titre}}</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <a href="/portrait/{{$item->id}}">{{$item->prenom}} &nbsp; {{$item->nom}}</a>
                      </li>
                      <li class="entry__meta-date">
                        {{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
              @endforeach
            </ul>
          </aside>
          <!-- end widget popular posts -->
          <!--  -->
          <!-- Widget Socials -->
          <aside class="widget widget-socials">
            <h4 class="widget-title">Suivez Nous </h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a><!--
                  --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a><!--
                  --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                </div>
                <div class="col">
                  <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                    <span class="social__text">Google+</span>
                  </a><!--
                  --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a><!--
                  --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                    <i class="ui-rss"></i>
                    <span class="social__text">Rss</span>
                  </a>
                </div>
              </div>
            </div>
          </aside> <!-- end widget socials -->

        </aside> <!-- end sidebar -->

      </div> <!-- end content -->
    </div> <!-- end main container -->

     <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')

</body>
</html>