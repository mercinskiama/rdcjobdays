
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Rdcjobdays | Accueil</title>
  <!--==========================
    Links
   ============================-->
   @include('structure/linkUp')

</head>

<body class="home style-politics">

  <!-- Preloader -->
  @include('structure/Preloader')

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->
  @include('structure/header')
  <!-- end sidenav -->


  <main class="main oh" id="main">

    <!-- Top Bar -->
      <div class="top-bar d-none d-lg-block">
          <div class="container">
            <div class="row">

              <!-- Top menu -->
              <div class="col-lg-6">
                <ul class="top-menu">
                  <li><a href="#">About</a></li>
                  <li><a href="#">Advertise</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>

              <!-- Socials -->
              <div class="col-lg-6">
                <div class="socials nav__socials socials--nobase socials--white justify-content-end">
                  <a class="social social-facebook" href="#" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                  </a>
                  <a class="social social-twitter" href="#" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                  </a>
                  <a class="social social-google-plus" href="#" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                  </a>
                  <a class="social social-youtube" href="#" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                  </a>
                  <a class="social social-instagram" href="#" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                  </a>
                </div>
              </div>

            </div>
          </div>
      </div>
      <!-- end top bar -->

    <!-- Navigation -->
    @include('structure/headerside')
    <!-- Navigation -->

    <!-- Trending Now -->
    <div class="container">
      <div class="trending-now" style="border-radius:5px;margin-bottom:2%;">
        <span class="trending-now__label">
          <i class="ui-flash"></i>
          <span class="trending-now__text d-lg-inline-block d-none">BREAKING NEWS</span>
        </span>
        <div class="newsticker">
          <ul class="newsticker__list">
            @foreach($breaknews as $news)
             <li class="newsticker__item"><a href="/Presente" style="font-weight:700;" class="newsticker__item-url">{{$news->titre_actu}}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="newsticker-buttons">
          <button class="newsticker-button newsticker-button--prev" id="newsticker-button--prev" aria-label="next article"><i class="ui-arrow-left"></i></button>
          <button class="newsticker-button newsticker-button--next" id="newsticker-button--next" aria-label="previous article"><i class="ui-arrow-right"></i></button>
        </div>
      </div>
    </div>

    <div class="main-container container" id="main-container">

      <!-- Content -->
      <div class="row row-20">

        <!-- Posts Actualité -->
        <div class="col-lg-6 order-lg-2">
          <section class="section mb-16">
              <article class="entry thumb thumb--size-3 thumb--mb-20">
                <div class="entry__img-holder thumb__img-holder" style="background-image: url({{url('media/img/content/hero/'.$principale->image1)}});">
                  <div class="bottom-gradient"></div>
                  <div class="thumb-text-holder thumb-text-holder--2">
                    <ul class="entry__meta">
                      <li>
                          <a href="/Presente/{{$principale->id}}" class="entry__meta-category">{{$principale->typeactualite->categorie}}</a>
                      </li>
                    </ul>
                    <h2 class="thumb-entry-title">
                      <a href="/Presente/{{$principale->id}}">{{$principale->titre_actu}}</a>
                    </h2>
                    <ul class="entry__meta">
                      <li class="entry__meta-views">
                        <i class="fa fa-user"></i>
                        <span>{{$principale->auteur}}</span>
                      </li>
                      <li class="entry__meta-comments">
                        <a href="/Presente/{{$principale->id}}">
                          <i class="fa fa-calendar"></i> {{\Carbon\Carbon::parse($principale->date_actu)->format('d M Y')}}
                        </a>
                      </li>
                    </ul>
                  </div>
                  <a href="/Presente/{{$principale->id}}" class="thumb-url"></a>
                </div>
              </article>

              <div class="row row-20">
                  @foreach($secondnews as $news)
                    <div class="col-md-6">
                      <article class="entry">
                        <div class="entry__img-holder">
                          <a href="/Presente/{{$news->id}}">
                            <div class="thumb-container thumb-65">
                              <img data-src="{{asset('media/img/content/hero/'.$news->image1)}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="" />
                            </div>
                          </a>
                        </div>

                        <div class="entry__body">
                          <div class="entry__header">
                            <ul class="entry__meta">
                              <li>
                                <a href="/Presente/{{$news->id}}" class="entry__meta-category">{{$news->typeactualite->categorie}}</a>
                              </li>
                            </ul>
                            <h2 class="entry__title">
                              <a href="/Presente/{{$news->id}}">{{$news->titre_actu}}</a>
                            </h2>
                          </div>
                          <div class="entry__excerpt">
                            <p>{{$news->second_actu}}</p>
                          </div>
                        </div>
                      </article>
                    </div>
                  @endforeach
              </div>

          </section>
        </div>
        <!-- End posts -->

        <!-- Sidebar Offre d'emplois -->
        <aside class="col-lg-3 sidebar order-lg-1">
          <!-- Widget Headlines -->
          <aside class="widget widget-headlines">
            <h4 class="widget-title d-flex justify-content-center">Recentes Offres</h4>
            <!-- Slider -->
            <div id="owl-headlines" class="owl-carousel owl-theme">
              <div class="owl-item">
                <ul class="post-list-small post-list-small--1">
                  @foreach($Newsemploi as $offre)
                    <li class="post-list-small__item">
                      <article class="post-list-small__entry clearfix">
                        <div class="post-list-small__body">
                          <h3 class="post-list-small__entry-title" >
                            <a href="#" style="font-weight:600;">{{$offre->titre}}</a>
                          </h3>
                          <span style="font-size:80%;">Publié: {{\Carbon\Carbon::parse($offre->date_publish)->format('d M Y')}}</span>
                        </div>
                      </article>
                    </li>
                  @endforeach
                </ul>
                <div class="d-flex justify-content-center">
                    {{ $Newsemploi->links() }}
                </div>
              </div>

            </div> <!-- end slider -->
          </aside> <!-- end widget headlines -->
        </aside>
        <!-- end sidebar -->

        <!-- Sidebar Tous les actualités -->
        <aside class="col-lg-3 sidebar order-lg-3">
          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title d-flex justify-content-center">Actualités recentes</h4>
            <ul class="post-list-small post-list-small--1">
              @foreach($recentActu as $recent)
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-80">
                      <a href="/Presente/{{$recent->id}}">
                        <img data-src="{{asset('media/img/content/hero/'.$recent->image1)}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="/Presente/{{$recent->id}}">{{$recent->titre_actu}}</a>
                    </h3>
                  </div>
                </article>
              </li>
              @endforeach
            </ul>
            <div class="d-flex justify-content-center">
              {{ $recentActu->links() }}
            </div>
          </aside> <!-- end widget popular posts -->
        </aside>
        <!-- end sidebar -->

      </div>
      <!-- end content -->
      <!--  -->

      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="title-wrap title-wrap--line">
            <h3 class="section-title">Echos des entreprises</h3>
          </div>

          <div class="row card-row">
            @foreach($pubs as $pub)
              <div class="col-md-6">
                <article class="entry card">
                  <div class="entry__img-holder card__img-holder">
                    <a href="single-post.html">
                      <div class="thumb-container thumb-70">
                        <img data-src="{{asset('media/img/content/pub/'.$pub->image)}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="" />
                      </div>
                    </a>
                    <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                  </div>

                  <div class="entry__body card__body">
                    <div class="entry__header">

                      <h2 class="entry__title">
                        <a href="single-post.html">{{$pub->titre}}</a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <span>by</span>
                          <a href="#">{{$pub->partenaire->designation}}</a>
                        </li>
                        <li class="entry__meta-date">
                          {{$pub->date}}
                        </li>
                      </ul>
                    </div>
                    <div class="entry__excerpt">
                      <p>{{$pub->seconde_titre}}</p>
                    </div>
                  </div>
                </article>
              </div>
            @endforeach
          </div>
          <!-- Pagination -->
          <div class="d-flex justify-content-center">
              {{$pubs->render()}}
          </div>

        </div> <!-- end posts -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Partenaire & Sponsors</h4>
            <ul class="post-list-small">
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="single-post.html">
                        <img data-src="{{asset('media/img/content/post_small/post_small_1.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                      <li class="entry__meta-date">
                        Jan 21, 2018
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="single-post.html">
                        <img data-src="{{asset('media/img/content/post_small/post_small_2.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="single-post.html">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                      <li class="entry__meta-date">
                        Jan 21, 2018
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="single-post.html">
                        <img data-src="{{asset('media/img/content/post_small/post_small_3.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="single-post.html">June in Africa: Taxi wars, smarter cities and increased investments</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                      <li class="entry__meta-date">
                        Jan 21, 2018
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="single-post.html">
                        <img data-src="{{asset('media/img/content/post_small/post_small_4.jpg')}}" src="{{asset('media/img/empty.png')}}" alt="" class=" lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="single-post.html">PUBG Desert Map Finally Revealed, Here Are All The Details</a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                      <li class="entry__meta-date">
                        Jan 21, 2018
                      </li>
                    </ul>
                  </div>
                </article>
              </li>
            </ul>
          </aside> <!-- end widget popular posts -->

          <!-- Widget Newsletter -->
          <aside class="widget widget_mc4wp_form_widget">
            <h4 class="widget-title">Newsletter</h4>
            <p class="newsletter__text">
              <i class="ui-email newsletter__icon"></i>
              Enregistrez-vous pour recevoir des offres et news
            </p>
            <form class="mc4wp-form" method="POST" action="/newsletter">
              {{csrf_field()}}
                <div class="mc4wp-form-fields">
                  <div class="form-group">
                    <input type="text" name="email" placeholder="Entrer votre adresse mail" required="">
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-color" value="Sign up">
                  </div>
                </div>
              </form>
          </aside> <!-- end widget newsletter -->

          <!-- Widget Socials -->
          <aside class="widget widget-socials">
            <h4 class="widget-title">Suivez nous sur les reseaux sociaux</h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a><!--
                  --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a><!--
                  --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                </div>
                <div class="col">
                  <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                    <span class="social__text">Google+</span>
                  </a><!--
                  --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a><!--
                  --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                    <i class="ui-rss"></i>
                    <span class="social__text">Rss</span>
                  </a>
                </div>
              </div>
            </div>
          </aside> <!-- end widget socials -->

        </aside>
        <!-- end sidebar -->
      </div>
       <!-- end content -->

      <!--  -->
      <!-- Carousel posts -->
      <section class="section mb-24">
        <div class="title-wrap title-wrap--line title-wrap--pr">
          <h3 class="section-title">Portrait</h3>
        </div>

        <!-- Slider -->
        <div id="owl-posts" class="owl-carousel owl-theme owl-carousel--arrows-outside">
          @foreach($portraits as $portait)
          <article class="entry">
            <div class="entry__img-holder">
              <a href="/portrait/{{$portait->id}}">
                <div class="thumb-container thumb-65">
                  <img data-src="{{asset('media/img/content/carousel/'.$portait->image1)}}" src="{{asset('media/img/empty.png')}}" class="entry__img lazyload" alt="">
                </div>
              </a>
            </div>
            <div class="entry__body">
              <div class="entry__header">
                <ul class="entry__meta">
                  <li>
                    <a href="/portrait/{{$portait->id}}" class="entry__meta-category">{{$portait->prenom}} &nbsp; {{$portait->nom}} &nbsp; {{$portait->postnom}}</a>
                  </li>
                </ul>
                <h2 class="entry__title">
                  <a href="/portrait/{{$portait->id}}">{{$portait->Titre}}</a>
                </h2>
              </div>
            </div>
          </article>
          @endforeach

        </div>
        <!-- end slider -->
      </section>
      <!-- end carousel posts -->

    </div>
    <!-- end main container -->

    <!--==========================
    Footer
    ============================-->
    @include('structure/footer')
    <!-- #footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->


  <!-- jQuery Scripts -->
  <!-- Link Bottom -->
  @include('structure/linkbottom')


</body>
</html>