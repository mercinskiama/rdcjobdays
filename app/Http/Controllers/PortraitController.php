<?php

namespace App\Http\Controllers;
use App\portrait;
use Illuminate\Http\Request;
use App\beans\enteteState;
use App\annonce;

class PortraitController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE DE PORTRAIT DE LA PLATEFORME
    public function showportrait($id) {

        //Gestion du cas Actif
        $state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('actualite');

        //Get One Portrait par son identifiant
        $infoportrait = portrait::where('id', '=', $id)->get();

        //Get tous les portraits
        $portraits = portrait::orderBy('id', 'DESC')->get();

        //Get Less Portrait
        $LessPortrait = portrait::orderBy('id', 'DESC')->take(5)->get();

        //Get Recentes Actu
        $recentActu = annonce::where('est_a_la_une', '=', 0)->orderBy('created_at', 'DESC')->paginate(4, ['*'], 'actu');

        return view('portrait')->with([
                                'EnteteState'=> $tableaudesEtats,
                                'infoportrait'=> $infoportrait[0],
                                'portraits'  => $portraits,
                                'recentActu' => $recentActu,
                                'LessPortrait' => $LessPortrait,
                              ]);
    }
}
