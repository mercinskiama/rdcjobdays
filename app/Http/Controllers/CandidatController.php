<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;

class CandidatController extends Controller
{
    //LES CONTROLLERS QUI GERE LES FONCTIONNALITE DE LA PAGE CANDADAT
    public function candidats(){
        //Gestion du cas Actif
        $state=new enteteState;
		$tableaudesEtats = $state->EtatDEntete('rdcjobdays');
        return view('candidat')->with([
                                    'EnteteState'=>$tableaudesEtats
                                    ]);
    }
}