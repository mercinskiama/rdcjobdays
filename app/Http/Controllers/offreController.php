<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;
use App\offre;


class offreController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE DES OFFRES D'EMPLOI DISPONIBLE DANS LA PLATEFORME
    public function offre(){
        //Gestion du cas Actif
        $state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('offre');

        //Get offre d'emploi
        $offre_emploi = offre::orderBy('id', 'desc')->paginate(7);

        //Get toutes les villes des emploi d'emploi
        $ville = offre::select('lieu')->distinct('lieu')->get();
        $domaine = offre::select('domaine')->distinct('domaine')->get();

        return view('offre')->with([
                                    'EnteteState'=>$tableaudesEtats,
                                    'offre_emploi' => $offre_emploi,
                                    'ville' => $ville,
                                    'domaine' => $domaine
                                    ]);
    }

    public function search(){

        //Gestion du cas Actif
        $state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('offre');

        // Traitement POST
        $getcategorie=$_POST["categorie"];$Searchcategorie  ='%'.$getcategorie.'%';
        $getville    =$_POST["ville"];$Searchville          ='%'.$getville.'%';
        $getdomaine  =$_POST["domaine"];$Searchdomaine      ='%'.$getdomaine.'%';

        $offre_emploi = offre::select('offres.*')
                    ->leftjoin('categorie_offres','categorie_offres.id','=','offres.categorie_offre_id')
                    ->where('categorie_offres.categorie','like', $Searchcategorie)
                    ->orWhere('offres.lieu','like',$Searchville)
                    ->orWhere('offres.domaine','like',$Searchdomaine)
                    ->orderBy('id', 'desc')
                    ->paginate(7);

       // dd($offre_emploi);

        return view('offre')->with([
                                'EnteteState'=>$tableaudesEtats,
                                'offre_emploi' => $offre_emploi
                                 ]);



    }
}
