<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;
use App\annonce;
use App\partenaire;
use App\offre;
use App\categorie_offre;
use App\publicite;
use App\portrait;
use Illuminate\Support\Facades\DB;

class ActualiteController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE D'ACTUALITE DE LA PLATEFORME

    public function callnews() {
        //Gestion du cas Actif
        $state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('actualite');

        //Get Breaking News data to DATABASE
        $breaknews = annonce::select('id','titre_actu')->where('est_a_la_une', '=', 1)->take(3)->get();

        //Get one info intrested news
        $news = annonce::where('est_a_la_une', '=', 1)->take(1)->get();
        //dd($news[0]->typeactualite->categorie);
        $secondnews = annonce::where('est_a_la_une', '=', 0)->orderBy('created_at', 'DESC')->take(2)->get();
        //dd($secondnews);

        //Get Recentes Actu
        $recentActu = annonce::where('est_a_la_une', '=', 0)->orderBy('created_at', 'DESC')->paginate(6, ['*'], 'published');

        //Get offre d'emploi
        $Newsemploi = offre::select('id','titre','date_publish')->orderBy('id', 'DESC')->paginate(7, ['*'], 'emploi');

        //Get Partenaire
        $partenaire = partenaire::orderBy('id', 'asc')->get();

        //Get Partenaire
        $pubs = publicite::orderBy('id', 'DESC')->paginate(4, ['*'], 'pub');

        //Get Portrait
        $portraits = portrait::orderBy('id', 'DESC')->get();

        return view('actualite')->with([
                                    'EnteteState'=>$tableaudesEtats,
                                    'breaknews' => $breaknews,
                                    'partenaire'=> $partenaire,
                                    'principale' => $news[0],
                                    'secondnews' => $secondnews,
                                    'recentActu' => $recentActu,
                                    'Newsemploi' => $Newsemploi,
                                    'pubs' => $pubs,
                                    'portraits' => $portraits,
                                    ]);
    }

    public function shownews($id) {
        //Gestion du cas Actif
        $state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('actualite');

        //Get une information par son id
        $information = annonce::where('id', '=', $id)->get();



        return view('shownews')->with([
                                    'EnteteState' => $tableaudesEtats,
                                    'information' => $information[0],
                                    ]);
    }






}
