<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE CONTACTS

    public function contact(){
        //Gestion du cas Actif
        $state=new enteteState;
		$tableaudesEtats = $state->EtatDEntete('rdcjobdays');
        return view('contact')->with([
                                    'EnteteState'=>$tableaudesEtats
                                    ]);
    }
}
