<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE ABOUT ET NEWSLETTERS DE LA PLATEFORME
    public function about(){
        //Gestion du cas Actif
        $state=new enteteState;
		$tableaudesEtats = $state->EtatDEntete('rdcjobdays');
        return view('about')->with([
                                    'EnteteState'=>$tableaudesEtats
                                    ]);
    }
}
