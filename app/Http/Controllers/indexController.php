<?php

namespace App\Http\Controllers;
use App\beans\enteteState;
use Illuminate\Http\Request;
use App\annonce;
use App\partenaire;
use App\categorie_offre;
use App\offre;
use Illuminate\Support\Facades\DB;

class indexController extends Controller
{
    //LES CONTROLLERS QUI GERE LA PAGE D'ACCUEIL DE LA PLATEFORME
    public function index(){
        //Gestion du cas Actif
		$state=new enteteState;
        $tableaudesEtats = $state->EtatDEntete('index');

        //Get Breaking News data to DATABASE
        $breaknews = annonce::select('id','titre_actu')->where('est_a_la_une', '=', 1)->take(3)->get();

        //Get one info intrested economie
        $economie = annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 2)->take(1)->get();

        //Get one info intrested securite
        $securite = annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 5)->take(1)->get();

        //Get one info intrested politique
        $politique = annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 1)->take(1)->get();

        //Get one info intrested societe
        $societe = annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 4)->take(1)->get();

        //Get one info intrested sport
        $sport = annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 3)->take(1)->get();

        //Get one info intrested sport
        $culture= annonce::select('id','titre_actu','image1')->where('typeactualite_id', '=', 6)->take(1)->get();

        //Get one info intrested news
        $news = annonce::select('id','titre_actu','image1', 'auteur', 'date_actu')->orderBy('created_at', 'DESC')->get();
        //$news = DB::table('annonces')->orderBy('created_at', 'DESC')->first();
        //dd($news);
        //Get Partenaire
        $partenaire = partenaire::orderBy('id', 'asc')->get();

        //Get categorie d'offre d'emploi
        $cat_offre = categorie_offre::orderBy('id', 'asc')->get();
        //Get offre d'emploi
        $offre_emploi = offre::where('categorie_offre_id', '=', 2)->orderBy('id', 'asc')->paginate(15);
        //Get Appel d'offre
        $appel_offre = offre::where('categorie_offre_id', '=', 1)->orderBy('id', 'asc')->paginate(15);

        //Get Nombre de offre par rapport au domaine d'emploi
        $Liste_emploi = offre::orderBy('id', 'asc')->get();


        return view('index')->with([
                                    'EnteteState'=>$tableaudesEtats,
                                    'breaknews' => $breaknews,
                                    'economie' => $economie,
                                    'news' => $news[0],
                                    'sport' => $sport,
                                    'societe' => $societe,
                                    'politique' => $politique,
                                    'securite' => $securite,
                                    'culture' => $culture,
                                    'partenaire'=> $partenaire,
                                    'cat_offre' => $cat_offre,
                                    'offre_emploi' => $offre_emploi,
                                    'appel_offre' => $appel_offre
                                    ]);
    }
}
