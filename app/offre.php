<?php

namespace App;
use App\categorie_offre;
use App\Traits\Orderable;
use Illuminate\Database\Eloquent\Model;

class offre extends Model
{
    //
    use Orderable;
    protected $guarded = [];

    public function offres()
    {
    	return $this->hasMany(offre::class);
    }
}
