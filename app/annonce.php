<?php

namespace App;
use App\typeactualite;

use Illuminate\Database\Eloquent\Model;

class annonce extends Model
{
    //
    protected $guarded = [];

    public function typeactualite()
    {
        return $this->belongsTo(typeactualite::class);
    }
}
