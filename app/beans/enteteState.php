<?php

namespace App\Beans;


use Illuminate\Notifications\Notifiable;

class enteteState
{
    public $tableauDesEtats=array();

    public function EtatDEntete($nomDelaPage)
    {

        $tableauDesEtats[0] = '';
        $tableauDesEtats[1] = '';
        $tableauDesEtats[2] = '';
        $tableauDesEtats[3] = '';
        $tableauDesEtats[4] = '';
        $tableauDesEtats[5] = '';

        if ($nomDelaPage=='index')
        {
            $tableauDesEtats[0] = 'active';
        }
        elseif ($nomDelaPage=='actualite')
        {
            $tableauDesEtats[1] = 'active';
        }
        elseif ($nomDelaPage == "offre")
        {
            $tableauDesEtats[2] = 'active';
        }
        elseif ($nomDelaPage == "rdcjobdays")
        {
            $tableauDesEtats[3] = 'active';
        }
        elseif ($nomDelaPage == "cvonline")
        {
            $tableauDesEtats[4] = 'active';
        }
        elseif ($nomDelaPage == "findin")
        {
            $tableauDesEtats[5] = 'active';
        }

        return $tableauDesEtats;

}
}