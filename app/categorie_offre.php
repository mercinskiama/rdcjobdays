<?php

namespace App;
use App\offre;
use App\Traits\Orderable;

use Illuminate\Database\Eloquent\Model;

class categorie_offre extends Model
{
    //
    use Orderable;
    protected $guarded = [];

    public function offres()
    {
    	return $this->hasMany(offre::class);
    }
}
