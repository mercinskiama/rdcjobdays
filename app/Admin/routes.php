<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('/annonce', ActualiteController::class);
    $router->resource('/user', UtilisateurController::class);
    $router->resource('/typeactu', TypeActuController::class);
    $router->resource('/pub', PubliciteController::class);
    $router->resource('/portrait', PortraitController::class);
    $router->resource('/partenaire', PartenaireController::class);
    $router->resource('/offre', OffreController::class);
    $router->resource('/newslettre', NewslettreController::class);
    $router->resource('/catoffre', CatOffreController::class);
    $router->resource('/candidat', CandidatController::class);
});
