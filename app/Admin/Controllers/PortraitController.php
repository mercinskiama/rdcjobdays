<?php

namespace App\Admin\Controllers;

use App\portrait;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PortraitController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion de portrait';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new portrait());

        $grid->column('id', __('Id'));
        $grid->column('nom', __('Nom'));
        $grid->column('postnom', __('Postnom'));
        $grid->column('prenom', __('Prenom'));
        $grid->column('date_naissance', __('Date naissance'));
        $grid->column('etatcivil', __('Etatcivil'));
        $grid->column('email', __('Email'));
        $grid->column('tel', __('Tel'));
        $grid->column('residence', __('Residence'));
        $grid->column('nationalite', __('Nationalite'));
        $grid->column('Qualification', __('Qualification'));
        $grid->column('Annee_Experience', __('Annee Experience'));
        // $grid->column('Titre', __('Titre'));
        // $grid->column('Article', __('Article'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('image1', __('Image1'));
        // $grid->column('image2', __('Image2'));
        // $grid->column('image3', __('Image3'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(portrait::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom', __('Nom'));
        $show->field('postnom', __('Postnom'));
        $show->field('prenom', __('Prenom'));
        $show->field('date_naissance', __('Date naissance'));
        $show->field('etatcivil', __('Etatcivil'));
        $show->field('email', __('Email'));
        $show->field('tel', __('Tel'));
        $show->field('residence', __('Residence'));
        $show->field('nationalite', __('Nationalite'));
        $show->field('Qualification', __('Qualification'));
        $show->field('Annee_Experience', __('Annee Experience'));
        $show->field('Titre', __('Titre'));
        $show->field('Article', __('Article'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('image1', __('Image1'));
        $show->field('image2', __('Image2'));
        $show->field('image3', __('Image3'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new portrait());

        $form->text('nom', __('Nom'));
        $form->text('postnom', __('Postnom'));
        $form->text('prenom', __('Prenom'));
        $form->datetime('date_naissance', __('Date naissance'))->default(date('Y-m-d H:i:s'));
        $form->text('etatcivil', __('Etatcivil'));
        $form->email('email', __('Email'));
        $form->number('tel', __('Tel'));
        $form->text('residence', __('Residence'));
        $form->text('nationalite', __('Nationalite'));
        $form->text('Qualification', __('Qualification'));
        $form->text('Annee_Experience', __('Annee Experience'));
        $form->text('Titre', __('Titre'));
        $form->textarea('Article', __('Article'));
        $form->text('image1', __('Image1'));
        $form->text('image2', __('Image2'));
        $form->text('image3', __('Image3'));

        return $form;
    }
}
