<?php

namespace App\Admin\Controllers;

use App\offre;
use App\categorie_offre;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class OffreController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion des offres d\'emploi';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new offre());

        $grid->column('id', __('Id'));
        $grid->column('titre', __('Titre'));
        // $grid->column('second_titre', __('Second titre'));
        // $grid->column('description_poste', __('Description poste'));
        // $grid->column('role', __('Role'));
        // $grid->column('responsabilite', __('Responsabilite'));
        // $grid->column('exigence', __('Exigence'));
        $grid->column('entreprise', __('Entreprise'));
        $grid->column('contrat', __('Contrat'));
        $grid->column('reference', __('Reference'));
        $grid->column('lieu', __('Lieu'));
        $grid->column('date_publish', __('Date publish'));
        $grid->column('date_fin', __('Date fin'));
        // $grid->column('autre_contenu1', __('Autre contenu1'));
        // $grid->column('autre_contenu2', __('Autre contenu2'));
        // $grid->column('autre_contenu3', __('Autre contenu3'));
        $grid->column('image1', __('Image1'));
        // $grid->column('image2', __('Image2'));
        $grid->column('poste', __('Poste'));
        $grid->column('Responsable', __('Responsable'));
        $grid->column('siege', __('Siege'));
        $grid->column('link', __('Link'));
        $grid->column('domaine', __('Domaine'));
        $grid->column('email', __('Email'));
        // $grid->column('autre', __('Autre'));
        $grid->column('etat', __('Etat'));
        $grid->column('categorie_offre_id', __('Categorie offre id'))->display(function($categorie_offre_id){
            $cat = categorie_offre::find($categorie_offre_id);
            return $cat->categorie;
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(offre::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('titre', __('Titre'));
        $show->field('second_titre', __('Second titre'));
        $show->field('description_poste', __('Description poste'));
        $show->field('role', __('Role'));
        $show->field('responsabilite', __('Responsabilite'));
        $show->field('exigence', __('Exigence'));
        $show->field('entreprise', __('Entreprise'));
        $show->field('contrat', __('Contrat'));
        $show->field('reference', __('Reference'));
        $show->field('lieu', __('Lieu'));
        $show->field('date_publish', __('Date publish'));
        $show->field('date_fin', __('Date fin'));
        $show->field('autre_contenu1', __('Autre contenu1'));
        $show->field('autre_contenu2', __('Autre contenu2'));
        $show->field('autre_contenu3', __('Autre contenu3'));
        $show->field('image1', __('Image1'));
        $show->field('image2', __('Image2'));
        $show->field('poste', __('Poste'));
        $show->field('Responsable', __('Responsable'));
        $show->field('siege', __('Siege'));
        $show->field('link', __('Link'));
        $show->field('domaine', __('Domaine'));
        $show->field('email', __('Email'));
        $show->field('autre', __('Autre'));
        $show->field('etat', __('Etat'));
        $show->field('categorie_offre_id', __('Categorie offre id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new offre());

        $form->textarea('titre', __('Titre'));
        $form->textarea('second_titre', __('Second titre'));
        $form->textarea('description_poste', __('Description poste'));
        $form->textarea('role', __('Role'));
        $form->textarea('responsabilite', __('Responsabilite'));
        $form->textarea('exigence', __('Exigence'));
        $form->text('entreprise', __('Entreprise'));
        $form->text('contrat', __('Contrat'));
        $form->text('reference', __('Reference'));
        $form->text('lieu', __('Lieu'));
        $form->datetime('date_publish', __('Date publish'))->default(date('Y-m-d H:i:s'));
        $form->datetime('date_fin', __('Date fin'))->default(date('Y-m-d H:i:s'));
        $form->text('autre_contenu1', __('Autre contenu1'));
        $form->text('autre_contenu2', __('Autre contenu2'));
        $form->text('autre_contenu3', __('Autre contenu3'));
        $form->text('image1', __('Image1'));
        $form->text('image2', __('Image2'));
        $form->text('poste', __('Poste'));
        $form->text('Responsable', __('Responsable'));
        $form->text('siege', __('Siege'));
        $form->url('link', __('Link'));
        $form->text('domaine', __('Domaine'));
        $form->email('email', __('Email'));
        $form->text('autre', __('Autre'));
        $form->text('etat', __('Etat'));
        $form->number('categorie_offre_id', __('Categorie offre id'));

        return $form;
    }
}
