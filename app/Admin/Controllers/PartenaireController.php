<?php

namespace App\Admin\Controllers;

use App\partenaire;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PartenaireController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion de partenaire';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new partenaire());

        $grid->column('id', __('Id'));
        $grid->column('designation', __('Designation'));
        $grid->column('image', __('Image'));
        // $grid->column('detail', __('Detail'));
        $grid->column('link', __('Link'));
        $grid->column('email', __('Email'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(partenaire::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('designation', __('Designation'));
        $show->field('image', __('Image'));
        $show->field('detail', __('Detail'));
        $show->field('link', __('Link'));
        $show->field('email', __('Email'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new partenaire());

        $form->text('designation', __('Designation'));
        $form->image('image', __('Image'));
        $form->text('detail', __('Detail'));
        $form->url('link', __('Link'));
        $form->email('email', __('Email'));

        return $form;
    }
}
