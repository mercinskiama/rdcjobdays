<?php

namespace App\Admin\Controllers;

use App\utilisateur;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UtilisateurController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion des utilisateurs';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new utilisateur());

        $grid->column('id', __('Id'));
        $grid->column('email', __('Email'));
        $grid->column('noms', __('Noms'));
        $grid->column('pwd', __('Pwd'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(utilisateur::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('email', __('Email'));
        $show->field('noms', __('Noms'));
        $show->field('pwd', __('Pwd'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new utilisateur());

        $form->email('email', __('Email'));
        $form->text('noms', __('Noms'));
        $form->password('pwd', __('Pwd'));

        return $form;
    }
}
