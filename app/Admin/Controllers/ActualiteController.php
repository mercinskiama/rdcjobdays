<?php

namespace App\Admin\Controllers;

use App\annonce;
use App\typeactualite;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ActualiteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion d\'actualités';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new annonce());

        $grid->column('id', __('Id'));
        $grid->column('titre_actu', __('Titre actu'));
        // $grid->column('second_actu', __('Second actu'));
        // $grid->column('contenu_actu', __('Contenu actu'));
        $grid->column('image1', __('Image1'));
        // $grid->column('image2', __('Image2'));
        // $grid->column('image3', __('Image3'));
        $grid->column('date_actu', __('Date actu'));
        $grid->column('source', __('Source'));
        $grid->column('link', __('Link'));
        $grid->column('auteur', __('Auteur'));
        $grid->column('est_a_la_une', __('Est a la une'));
        $grid->column('typeactualite_id', __('Typeactualite'))->display(function($typeactualite_id)
        {
            $typeA = typeactualite::find($typeactualite_id);
            return $typeA->categorie;
        }
        );
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(annonce::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('titre_actu', __('Titre actu'));
        $show->field('second_actu', __('Second actu'));
        $show->field('contenu_actu', __('Contenu actu'));
        $show->field('image1', __('Image1'));
        $show->field('image2', __('Image2'));
        $show->field('image3', __('Image3'));
        $show->field('date_actu', __('Date actu'));
        $show->field('source', __('Source'));
        $show->field('link', __('Link'));
        $show->field('auteur', __('Auteur'));
        $show->field('est_a_la_une', __('Est a la une'));
        $show->field('typeactualite_id', __('Typeactualite id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new annonce());

        $form->textarea('titre_actu', __('Titre actu'));
        $form->textarea('second_actu', __('Second actu'));
        $form->textarea('contenu_actu', __('Contenu actu'));
        $form->text('image1', __('Image1'));
        $form->text('image2', __('Image2'));
        $form->text('image3', __('Image3'));
        $form->datetime('date_actu', __('Date actu'))->default(date('Y-m-d H:i:s'));
        $form->text('source', __('Source'));
        $form->url('link', __('Link'));
        $form->text('auteur', __('Auteur'));
        $form->switch('est_a_la_une', __('Est a la une'));
        $form->number('typeactualite_id', __('Typeactualite id'));

        return $form;
    }
}
