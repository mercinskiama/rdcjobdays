<?php

namespace App\Admin\Controllers;

use App\candidat;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CandidatController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion des candidats';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new candidat());

        $grid->column('id', __('Id'));
        $grid->column('nom', __('Nom'));
        $grid->column('postnom', __('Postnom'));
        $grid->column('prenom', __('Prenom'));
        $grid->column('date_naissance', __('Date naissance'));
        $grid->column('sexe', __('Sexe'));
        $grid->column('etatcivil', __('Etatcivil'));
        $grid->column('email', __('Email'));
        $grid->column('tel', __('Tel'));
        $grid->column('residence', __('Residence'));
        $grid->column('nationalite', __('Nationalite'));
        // $grid->column('Qualification', __('Qualification'));
        $grid->column('Annee_Experience', __('Annee Experience'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(candidat::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom', __('Nom'));
        $show->field('postnom', __('Postnom'));
        $show->field('prenom', __('Prenom'));
        $show->field('date_naissance', __('Date naissance'));
        $show->field('sexe', __('Sexe'));
        $show->field('etatcivil', __('Etatcivil'));
        $show->field('email', __('Email'));
        $show->field('tel', __('Tel'));
        $show->field('residence', __('Residence'));
        $show->field('nationalite', __('Nationalite'));
        $show->field('Qualification', __('Qualification'));
        $show->field('Annee_Experience', __('Annee Experience'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new candidat());

        $form->text('nom', __('Nom'));
        $form->text('postnom', __('Postnom'));
        $form->text('prenom', __('Prenom'));
        $form->datetime('date_naissance', __('Date naissance'))->default(date('Y-m-d H:i:s'));
        $form->text('sexe', __('Sexe'));
        $form->text('etatcivil', __('Etatcivil'));
        $form->email('email', __('Email'));
        $form->number('tel', __('Tel'));
        $form->text('residence', __('Residence'));
        $form->text('nationalite', __('Nationalite'));
        $form->text('Qualification', __('Qualification'));
        $form->text('Annee_Experience', __('Annee Experience'));

        return $form;
    }
}
