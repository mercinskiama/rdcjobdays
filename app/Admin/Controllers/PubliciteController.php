<?php

namespace App\Admin\Controllers;

use App\publicite;
use App\partenaire;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PubliciteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gestion de publicité';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new publicite());

        $grid->column('id', __('Id'));
        $grid->column('titre', __('Titre'));
        // $grid->column('seconde_titre', __('Seconde titre'));
        $grid->column('image', __('Image'));
        $grid->column('link', __('Link'));
        $grid->column('date', __('Date'));
        $grid->column('partenaire_id', __('Partenaire id'))->display(function($partenaire_id){
            $pat = partenaire::find($partenaire_id);
            return $pat->designation;
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(publicite::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('titre', __('Titre'));
        $show->field('seconde_titre', __('Seconde titre'));
        $show->field('image', __('Image'));
        $show->field('link', __('Link'));
        $show->field('date', __('Date'));
        $show->field('partenaire_id', __('Partenaire id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new publicite());

        $form->text('titre', __('Titre'));
        $form->text('seconde_titre', __('Seconde titre'));
        $form->image('image', __('Image'));
        $form->url('link', __('Link'));
        $form->datetime('date', __('Date'))->default(date('Y-m-d H:i:s'));
        $form->number('partenaire_id', __('Partenaire id'));

        return $form;
    }
}
