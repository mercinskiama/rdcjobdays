<?php

namespace App;
use App\annonce;
use App\Traits\Orderable;

use Illuminate\Database\Eloquent\Model;

class typeactualite extends Model
{
    //
    use Orderable;
    protected $guarded = [];

    public function annonces()
    {
    	return $this->hasMany(annonce::class);
    }
}
