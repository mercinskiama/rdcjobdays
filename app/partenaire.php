<?php

namespace App;
use App\publicite;
use App\Traits\Orderable;

use Illuminate\Database\Eloquent\Model;

class partenaire extends Model
{
    //
    use Orderable;
    protected $guarded = [];

    public function publicites()
    {
    	return $this->hasMany(publicite::class);
    }
}
