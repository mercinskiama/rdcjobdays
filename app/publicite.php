<?php

namespace App;
use App\partenaire;

use Illuminate\Database\Eloquent\Model;

class publicite extends Model
{
    //
    protected $guarded = [];

    public function partenaire()
    {
        return $this->belongsTo(partenaire::class);
    }
}
