<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'indexController@index');

// ACTUALITE & ACCUEIL
Route::get('/', 'ActualiteController@callnews');
Route::get('/Presente/{id}', 'ActualiteController@shownews');

// OFFRES
Route::get('/offre', 'offreController@offre');
Route::post('/search', 'offreController@search');

Route::get('/about', 'AboutController@about');
Route::get('/contact', 'ContactController@contact');
Route::get('/candidat', 'CandidatController@candidats');

Route::get('/portrait/{id}', 'PortraitController@showportrait');


Route::post('/newsletter', 'newslettreController@savetonewsletter');
