<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('titre')->nullable();
            $table->longText('second_titre')->nullable();
            $table->longText('description_poste')->nullable();
            $table->longText('role')->nullable();
            $table->longText('responsabilite')->nullable();
            $table->longText('exigence')->nullable();
            $table->string('entreprise', 250)->nullable();
            $table->string('contrat', 250)->nullable();
            $table->string('reference', 250)->nullable();
            $table->string('lieu', 250)->nullable();
            $table->dateTime('date_publish')->nullable();
            $table->dateTime('date_fin')->nullable();
            $table->string('autre_contenu1', 250)->nullable();
            $table->string('autre_contenu2', 250)->nullable();
            $table->string('autre_contenu3', 250)->nullable();
            $table->string('image1', 250)->nullable();
            $table->string('image2', 250)->nullable();
            $table->string('poste', 250)->nullable();
            $table->string('Responsable', 250)->nullable();
            $table->string('siege', 250)->nullable();
            $table->string('link', 250)->nullable();
            $table->string('domaine', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->string('autre', 250)->nullable();
            $table->enum('etat', array('encours', 'expire', 'non precise'));
            $table->integer('categorie_offre_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('categorie_offre_id')->references('id')->on('categorie_offres')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
