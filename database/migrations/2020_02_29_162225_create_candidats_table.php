<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 250)->nullable();
            $table->string('postnom', 250)->nullable();
            $table->string('prenom', 250)->nullable();
            $table->dateTime('date_naissance')->nullable();
            $table->char('sexe', 4)->nullable();
            $table->string('etatcivil', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('tel')->nullable();
            $table->string('residence', 250)->nullable();
            $table->string('nationalite', 250)->nullable();
            $table->string('Qualification', 250)->nullable();
            $table->string('Annee_Experience', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidats');
    }
}
