<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnoncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('titre_actu')->nullable();
            $table->longText('second_actu')->nullable();
            $table->longText('contenu_actu')->nullable();
            $table->string('image1',250)->nullable();
            $table->string('image2',250)->nullable();
            $table->string('image3',250)->nullable();
            $table->dateTime('date_actu')->nullable();
            $table->string('source',250)->nullable();
            $table->string('link',250)->nullable();
            $table->string('auteur',100)->nullable();
            $table->tinyInteger('est_a_la_une')->default('0');
            $table->integer('typeactualite_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('typeactualite_id')->references('id')->on('typeactualites')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
