<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre', 250)->nullable();
            $table->string('seconde_titre', 250)->nullable();
            $table->string('image', 250)->nullable();
            $table->string('link', 250)->nullable();
            $table->dateTime('date')->nullable();
            $table->integer('partenaire_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('partenaire_id')->references('id')->on('partenaires')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicites');
    }
}
